#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include "options.h"
#include "rtree-build.h"

int wrap(struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  rtree_build_t opt;

  opt.verbose = info->verbose_given;
  opt.outfile = (info->output_given ? info->output_arg : NULL);

  if (info->dimension_given)
    opt.dimension = info->dimension_arg;
  else
    {
      fprintf(stderr, "dimension must be specified\n");
      return EXIT_FAILURE;
    }

  if (!opt.outfile && opt.verbose)
    {
      fprintf(stderr, "verbosity suppressed (<stdout> is used for output)\n");
      opt.verbose = 0;
    }

  switch (info->split_arg)
    {
    case split_arg_linear:
      opt.split = split_linear;
      break;
    case split_arg_quadratic:
      opt.split = split_quadratic;
      break;
    case split_arg_greene:
      opt.split = split_greene;
      break;
    default:
      return EXIT_FAILURE;
    }

  opt.node_page = info->nodes_per_page_arg;

  switch (info->format_arg)
    {
    case format_arg_json:
      opt.format = format_json;
      break;
    case format_arg_bsrt:
      opt.format = format_bsrt;
      break;
    default:
      return EXIT_FAILURE;
    }

  switch (info->inputs_num)
    {
    case 0:
      opt.infile = NULL;
      break;

    case 1:
      opt.infile = info->inputs[0];
      break;

    default:
      fprintf(stderr,"sorry, only one file at a time\n");
      return EXIT_FAILURE;
    }

  if (opt.verbose)
    printf("This is rtree-build (version %s)\n", VERSION);

  int err = rtree_build(&opt);

  if (err)
    fprintf(stderr, "failed\n");
  else if (opt.verbose)
    {
      printf("rtree written to %s\n",
             (opt.outfile ? opt.outfile : "<stdout>"));
      printf("done.\n");
    }

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;

  if (options(argc, argv, &info) != 0)
    {
      fprintf(stderr, "failed to parse command line\n");
      return EXIT_FAILURE;
    }

  int err = wrap(&info);

  options_free(&info);

  return err;
}
