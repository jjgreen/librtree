/*
  private/split.h
  Copyright (c) J.J. Green 2020
*/

#ifndef PRIVATE_SPLIT_H
#define PRIVATE_SPLIT_H

#include <private/branch.h>
#include <private/node.h>
#include <private/state.h>

node_t* split_node(const state_t*, node_t*, branch_t*);

#endif
