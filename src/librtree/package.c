/*
  Generated from package.c.in, do not edit (but do not delete,
  this file is checked into version control)
*/

#include "rtree/package.h"

const char rtree_package_version[] = "1.3.2";
const char rtree_package_name[] = "librtree";
const char rtree_package_url[] = "https://gitlab.com/jjg/librtree";
const char rtree_package_bugreport[] = "j.j.green@gmx.co.uk";
