/*
  rtree/state.h

  This structure is open, but we only access it via accessors,
  often trivial; these are implemented as (C99 standard) inline
  functions, so there's no performance penalty.

  Copyright (c) J.J. Green 2019
*/

#ifndef RTREE_STATE_H
#define RTREE_STATE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef uint32_t state_flags_t;

#define RTREE_DEFAULT 0

#define RTREE_SPLIT_QUADRATIC 0
#define RTREE_SPLIT_LINEAR 1
#define RTREE_SPLIT_GREENE 2

#define RTREE_NODE_PAGE(n) ((n) << 2)

#ifdef __cplusplus
}
#endif

#endif
