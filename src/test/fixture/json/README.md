JSON fixtures
-------------

The JSON format read and written by the library depends on the
size of rtree_coord_t, the rectangle coordinates.  The output
from a double version of the library cannot be read by a float
version, and vice versa.

Consequently, we need per-type versions of the fixtures for the
tests, these are in the obviously named subdirectories.
