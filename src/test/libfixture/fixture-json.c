#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef WITH_JSON

#include "fixture-json.h"
#include "fixture.h"

#define TPLEN 512

static const char* type_path(const char *file)
{
  static char path[TPLEN];

#if SIZEOF_RTREE_COORD_T == 4
  const char dir[] = "float";
#elif SIZEOF_RTREE_COORD_T == 8
  const char dir[] = "double";
#endif

  if (snprintf(path, TPLEN, "%s/%s", dir, file) >= TPLEN)
    return NULL;

  return path;
}

/* a json_t* loaded from fixture/json/<type>/<file> */

json_t* fixture_json_jansson(const char *file)
{
  size_t n = 1024;
  char path[n];

  if (fixture(path, n, "json", type_path(file)) >= (int)n)
    return NULL;

  json_error_t error;
  json_t *json = json_load_file(path, 0, &error);

  if (json == NULL)
    {
      printf("error: line %i, column %i\n", error.line, error.column);
      printf("%s\n", error.text);
      return NULL;
    }

  return json;
}

FILE* fixture_json_stream(const char *file)
{
  size_t n = 1024;
  char path[n];

  if (fixture(path, n, "json", type_path(file)) >= (int)n)
    return NULL;

  return fopen(path, "r");
}

#endif
