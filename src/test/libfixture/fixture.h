/*
  fixture.h
  Copyright (c) J.J. Green 2020
*/

#ifndef FIXTURE_H
#define FIXTURE_H

#include <stdlib.h>

int fixture(char*, size_t, const char*, const char*);

#endif
