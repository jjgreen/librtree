/*
  fixture_bsrt.h
  Copyright (c) J.J. Green 2019
*/

#ifndef FIXTURE_BSRT_H
#define FIXTURE_BSRT_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "rtree.h"

#include <stdio.h>

rtree_t* fixture_bsrt(const char*);
FILE* fixture_bsrt_stream(const char*);

#endif
