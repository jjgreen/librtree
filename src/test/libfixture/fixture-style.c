#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fixture-style.h"
#include "fixture.h"


postscript_style_t* fixture_style(const char *file)
{
  size_t n = 1024;
  char path[n];

  if (fixture(path, n, "style", file) >= (int)n)
    return NULL;

  FILE *stream;

  if ((stream = fopen(path, "r")) != NULL)
    {
      postscript_style_t *style = postscript_style_read(stream);
      fclose(stream);
      return style;
    }

  return NULL;
}
