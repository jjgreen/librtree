#include <stdio.h>
#include "fixture.h"

#define FIXTURE_BASE "../fixture"

int fixture(char *path, size_t n, const char *type, const char *file)
{
  return snprintf(path, n, "%s/%s/%s", FIXTURE_BASE, type, file);
}
