/*
  fixture_style.h
  Copyright (c) J.J. Green 2020
*/

#ifndef FIXTURE_STYLE_H
#define FIXTURE_STYLE_H

#include "rtree/postscript.h"

postscript_style_t* fixture_style(const char*);

#endif
