#!/usr/bin/env bats

load 'config'
load 'shared'

setup()
{
    program='rtree-build'
    path="${src_dir}/${program}/${program}"
}

@test 'rtree-build, --version' {
    run $path --version
    [ "${status}" -eq 0 ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'rtree-build, --help' {
    run $path --help
}

@test 'rtree-build, --unknown' {
    run $path --unknown
    [ "${status}" -ne 0 ]
}

@test 'rtree-build, simple-lf, json' {
    csv="${fixture_dir}/csv/simple-lf.csv"
    json="${BATS_TEST_TMPDIR}/simple-lf.json"
    run $path -d 2 -f json -o $json $csv
    [ "${status}" -eq 0 ]
    [ -e "${json}" ]
}

@test 'rtree-build, simple-crlf, json' {
    csv="${fixture_dir}/csv/simple-crlf.csv"
    json="${BATS_TEST_TMPDIR}/build-simple-lf.json"
    run $path -d 2 -f json -o $json $csv
    [ "${status}" -eq 0 ]
    [ -e "${json}" ]
}

@test 'rtree-build, simple-lf, bsrt' {
    csv="${fixture_dir}/csv/simple-lf.csv"
    bsrt="${BATS_TEST_TMPDIR}/build-simple-lf.bsrt"
    run $path -d 2 -f bsrt -o $bsrt $csv
    [ "${status}" -eq 0 ]
    [ -e "${bsrt}" ]
}

@test 'rtree-build, simple-crlf, bsrt' {
    csv="${fixture_dir}/csv/simple-crlf.csv"
    bsrt="${BATS_TEST_TMPDIR}/build-simple-lf.bsrt"
    run $path -d 2 -f bsrt -o $bsrt $csv
    [ "${status}" -eq 0 ]
    [ -e "${bsrt}" ]
}

@test 'rtree-build, --verbose' {
    csv="${fixture_dir}/csv/simple-lf.csv"
    json="${BATS_TEST_TMPDIR}/build-verbose.json"
    run $path -v -d 2 -f json -o $json $csv
    [ "${status}" -eq 0 ]
    [ -e "${json}" ]
}

@test 'rtree-build, too many input files' {
    csv="${fixture_dir}/csv/simple-crlf.csv"
    bsrt="${BATS_TEST_TMPDIR}/build-too-many-inputs.bsrt"
    run $path -d 2 -f bsrt -o $bsrt $csv $csv
    [ "${status}" -ne 0 ]
    [ ! -e "${bsrt}" ]
}

@test 'rtree-build, input file does not exist' {
    csv="${fixture_dir}/csv/no-such-file.csv"
    bsrt="${BATS_TEST_TMPDIR}/build-no-such-input.bsrt"
    run $path -d 2 -f bsrt -o $bsrt $csv
    [ "${status}" -ne 0 ]
    [ ! -e "${bsrt}" ]
}

@test 'rtree-build, read stdin' {
    csv="${fixture_dir}/csv/simple-crlf.csv"
    bsrt="${BATS_TEST_TMPDIR}/build-read-stdin.bsrt"
    run $path -d 2 -f bsrt -o $bsrt < $csv
    [ "${status}" -eq 0 ]
    [ -e "${bsrt}" ]
}

@test 'rtree-build, write stdout' {
    csv="${fixture_dir}/csv/simple-lf.csv"
    json="${BATS_TEST_TMPDIR}/build-write-stdout.json"
    run $path -v -d 2 -f json $csv > $json
    [ "${status}" -eq 0 ]
    [ -e "${json}" ]
}

@test 'rtree-build, write inexistent directory' {
    csv="${fixture_dir}/csv/simple-lf.csv"
    json="${BATS_TEST_TMPDIR}/build-no-such/no-such.json"
    run $path -v -d 2 -f json -o $json $csv
    [ "${status}" -ne 0 ]
    [ ! -e "${json}" ]
}

@test 'rtree-build, bad format' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/build-bad-format.json"
    run $path -d 2 -f unknown -o $json $csv
    [ "${status}" -ne 0 ]
    [ ! -e "${json}" ]
}

@test 'rtree-build, split linear' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/build-split-square.json"
    run $path -v -d 2 -f json -s linear -o $json $csv
    [ "${status}" -eq 0 ]
    [ -e "${json}" ]
}

@test 'rtree-build, split quadratic' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/build-split-quadratic.json"
    run $path -v -d 2 -f json -s quadratic -o $json $csv
    [ "${status}" -eq 0 ]
    [ -e "${json}" ]
}

@test 'rtree-build, split Greene' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/build-split-greene.json"
    run $path -v -d 2 -f json -s greene -o $json $csv
    [ "${status}" -eq 0 ]
    [ -e "${json}" ]
}

@test 'rtree-build, split unknown' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/build-split-unknown.json"
    run $path -v -d 2 -f json -s unknown -o $json $csv
    [ "${status}" -ne 0 ]
    [ ! -e "${json}" ]
}

@test 'rtree-build, no dimension' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/build-no-dimension.json"
    run $path -f json -s linear -o $json $csv
    [ "${status}" -ne 0 ]
    [ ! -e "${json}" ]
}
