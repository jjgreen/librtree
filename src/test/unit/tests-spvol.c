#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-spvol.h"

#include <private/spvol.h>

#include <float.h>

CU_TestInfo tests_spvol[] = {
  {"zero", test_spvol_zero},
  {"large", test_spvol_large},
  {"small", test_spvol_small},
  CU_TEST_INFO_NULL
};

void test_spvol_zero(void)
{
  double v = 0.0;
  CU_ASSERT_NOT_EQUAL(spvol(0, &v), 0);
  CU_ASSERT_EQUAL(v, 0.0);
}

void test_spvol_large(void)
{
  double v = 0.0;
  CU_ASSERT_NOT_EQUAL(spvol(33, &v), 0);
  CU_ASSERT_EQUAL(v, 0.0);
}

/*
  Compare against values calculated from the standard formulae,
  evaluated with PARI/GP, we seek relative accuracy of twice
  DBL_EPSILON, note that CU_ASSERT_DOUBLE_EQUAL assert absolute
  accuracy.
*/

static void assert_dim_volume(size_t n, double v0)
{
  double v1,  eps = 2 * DBL_EPSILON;
  CU_ASSERT_EQUAL(spvol(n, &v1), 0);
  CU_ASSERT_DOUBLE_EQUAL(v0, v1, v0 * eps);
}

#define CU_ASSERT_DIM_VOLUME(dim, v)  assert_dim_volume(dim, v)

void test_spvol_small(void)
{
  CU_ASSERT_DIM_VOLUME(1, 2.00000000000000000);
  CU_ASSERT_DIM_VOLUME(2, 3.14159265358979324);
  CU_ASSERT_DIM_VOLUME(3, 4.18879020478639098);
  CU_ASSERT_DIM_VOLUME(4, 4.93480220054467931);
  CU_ASSERT_DIM_VOLUME(5, 5.26378901391432460);
  CU_ASSERT_DIM_VOLUME(6, 5.16771278004997003);
  CU_ASSERT_DIM_VOLUME(7, 4.72476597033140117);
  CU_ASSERT_DIM_VOLUME(8, 4.05871212641676822);
  CU_ASSERT_DIM_VOLUME(9, 3.29850890273870687);
}
