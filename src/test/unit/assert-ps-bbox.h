/*
  assert_ps_hrbbox.h

  Checks that the high-resolution bounding-box of the
  path to postscript is within the specified accuracy.

  Copyright (c) J.J. Green 2020
*/

#ifndef ASSERT_PS_HRBBOX_H
#define ASSERT_PS_HRBBOX_H

#define CU_ASSERT_PS_BBOX(path, bbox, eps) assert_ps_bbox(path, bbox, eps)

void assert_ps_bbox(const char*, const double[static 4], double);

#endif
