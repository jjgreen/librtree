#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-rtree.h"
#include "assert-dyadic-power.h"
#include "fixture-json.h"
#include "fixture-csv.h"

#include <private/branch.h>
#include <private/node.h>
#include <private/rtree.h>

#include <float.h>

CU_TestInfo tests_rtree[] = {
  {"new", test_rtree_new},
  {"destroy", test_rtree_destroy},
  {"add rect", test_rtree_add_rect},
  {"update, empty", test_rtree_update_empty},
  {"update, single", test_rtree_update_single},
  {"update, multiple", test_rtree_update_multiple},
  {"search", test_rtree_search},
  {"clone", test_rtree_clone},
  {"split horizontal", test_rtree_split_horizontal},
  {"split vertical", test_rtree_split_vertical},
  {"guttman", test_rtree_guttman},
  {"bytes, NULL", test_rtree_bytes_null},
  {"bytes, non-NULL", test_rtree_bytes_not_null},
  {"dims, NULL", test_rtree_dims_null},
  {"dims, non-NULL", test_rtree_dims_not_null},
  {"page_size, NULL", test_rtree_page_size_null},
  {"page_size, non-NULL", test_rtree_page_size_not_null},
  {"node_size, NULL", test_rtree_node_size_null},
  {"node_size, non-NULL", test_rtree_node_size_not_null},
  {"rect_size, NULL", test_rtree_rect_size_null},
  {"rect_size, non-NULL", test_rtree_rect_size_not_null},
  {"branch_size, NULL", test_rtree_branch_size_null},
  {"branch_size, non-NULL", test_rtree_branch_size_not_null},
  {"branching_factor, NULL", test_rtree_branching_factor_null},
  {"branching_factor, non-NULL", test_rtree_branching_factor_not_null},
  {"unit_sphere_volume, NULL", test_rtree_unit_sphere_volume_null},
  {"unit_sphere_volume, non-NULL", test_rtree_unit_sphere_volume_not_null},
  {"empty, NULL", test_rtree_empty_null},
  {"empty, non-NULL", test_rtree_empty_not_null},
  {"empty, split", test_rtree_empty_split},
  {"envelope, NULL", test_rtree_envelope_null},
  {"envelope, empty", test_rtree_envelope_empty},
  {"envelope, single", test_rtree_envelope_single},
  {"envelope, nested", test_rtree_envelope_nested},
  {"envelope, cross", test_rtree_envelope_cross},
  CU_TEST_INFO_NULL
};

/* rtree_new */

void test_rtree_new(void)
{
  for (size_t n = 1 ; n <= 10 ; n++)
    {
      rtree_t *rtree = rtree_new(n, 0);

      CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
      CU_ASSERT_PTR_NOT_NULL(rtree->state);
      CU_ASSERT_PTR_NOT_NULL(rtree->root);
      CU_ASSERT_EQUAL(rtree_height(rtree), 0);

      rtree_destroy(rtree);
    }

  CU_ASSERT_PTR_NULL(rtree_new(0, 0));
}

/* rtree_destroy */

void test_rtree_destroy(void)
{
  rtree_destroy(NULL);
}

/* rtree_add_rect */

void test_rtree_add_rect(void)
{
  size_t dims = 2;
  rtree_coord_t rect[] = {
    1.0, 2.0,
    2.0, 3.0
  };
  rtree_t *rtree = rtree_new(dims, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(rtree_height(rtree), 0);
  CU_ASSERT_EQUAL(rtree_add_rect(rtree, 3, rect), 0);
  CU_ASSERT_EQUAL(rtree_height(rtree), 1);

  rtree_destroy(rtree);
}

/* rtree_update */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
static int shift_right(rtree_id_t id, rtree_coord_t *rect, void *context)
{
  rect[0]++; rect[2]++;
  return 0;
}
#pragma GCC diagnostic pop

void test_rtree_update_empty(void)
{
  size_t dims = 2;
  rtree_t *rtree = rtree_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  int err = rtree_update(rtree, shift_right, NULL);
  CU_ASSERT_EQUAL(err, 0);

  rtree_destroy(rtree);
}

void test_rtree_update_single(void)
{
  size_t dims = 2;
  rtree_t *rtree= rtree_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  int err;
  rtree_coord_t rect[4] = {0, 0, 1, 1};

  err = rtree_add_rect(rtree, 43, rect);
  CU_ASSERT_EQUAL(err, 0);

  err = rtree_update(rtree, shift_right, NULL);
  CU_ASSERT_EQUAL(err, 0);

  rtree_destroy(rtree);
}

/*
  find_only() is used as a search function, if it finds anything
  but 'expected' then it returns 42.  We use a probe rectangle
  which intersects only rectangle 3 of the fisture, and check
  with find_only that this is the case, we then apply shift right,
  which one would expect would cause the probe rectangle to now
  only intersect rectangle 2 of the fixture, and we again check
  that this is the case.
*/

static int find_only(rtree_id_t id, void *context)
{
  rtree_id_t *expected = (rtree_id_t*)context;
  if (*expected == id)
    return 0;
  else
    return 42;
}

void test_rtree_update_multiple(void)
{
  rtree_t *rtree = fixture_csv_rtree(2, 0, "split-horizontal.csv");
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  int err;
  rtree_id_t expected;
  rtree_coord_t probe[4] = {3.1, 0.3, 3.4, 0.7};

  expected = 2;
  err = rtree_search(rtree, probe, find_only, &expected);
  CU_ASSERT_EQUAL(err, 42);
  expected = 3;
  err = rtree_search(rtree, probe, find_only, &expected);
  CU_ASSERT_EQUAL(err, 0);

  err = rtree_update(rtree, shift_right, NULL);
  CU_ASSERT_EQUAL(err, RTREE_OK);

  expected = 2;
  err = rtree_search(rtree, probe, find_only, &expected);
  CU_ASSERT_EQUAL(err, 0);
  expected = 3;
  err = rtree_search(rtree, probe, find_only, &expected);
  CU_ASSERT_EQUAL(err, 42);

  rtree_destroy(rtree);
}

/* rtree_search */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
static int count(unsigned long id, void *arg)
{
  (*((int*)arg))++;
  return 0;
}
#pragma GCC diagnostic pop

void rtree_search_flag(unsigned flags)
{
  rtree_t *rtree = fixture_csv_rtree(2, flags, "single-square.csv");
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(rtree_height(rtree), 1);

  rtree_coord_t rect[] = {1.25, 2.25, 1.75, 2.75};
  unsigned long n = 0;
  int err = rtree_search(rtree, rect, count, &n);

  CU_ASSERT_EQUAL(err, 0);
  CU_ASSERT_EQUAL(n, 1);

  rtree_destroy(rtree);
}

void test_rtree_search(void)
{
  rtree_search_flag(RTREE_SPLIT_LINEAR);
  rtree_search_flag(RTREE_SPLIT_QUADRATIC);
  rtree_search_flag(RTREE_SPLIT_GREENE);
}


/* rtree_clone */

void test_rtree_clone(void)
{
  rtree_t *rtree = fixture_csv_rtree(2, 0, "split-square.csv");
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  rtree_t *clone = rtree_clone(rtree);
  CU_ASSERT_PTR_NOT_NULL_FATAL(clone);
  CU_ASSERT_TRUE(rtree_identical(rtree, clone));
  rtree_destroy(clone);
  rtree_destroy(rtree);
}


/*
  Tests for "reasonable" splitting: we need to add 128 rectangle
  to force a split, and when we chose a sequence of disjoint
  rectangles in a line, it's reasonable to expect that the split
  generated disjoint envelopes.

  In the double case SIZEOF_RTREE_COORD_T = 8, it just so happens
  that we gat a 3-way split, and in that case we check pairwise
  disjointness
*/

typedef struct
{
  branch_t const **array;
  size_t count;
} gather_t;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
static int branch_gather_f(const state_t *state, branch_t const *branch, void *arg)
{
  gather_t *gather = arg;
  if (gather->count == 0) return 1;
  gather->count--;
  gather->array[gather->count] = branch;

  return 0;
}
#pragma GCC diagnostic pop

static void rtree_split_horizontal_flag(unsigned flags)
{
  rtree_t *rtree = fixture_csv_rtree(2, flags, "split-horizontal.csv");
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(rtree_height(rtree), 2);

  node_t *root = rtree->root;
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

#if SIZEOF_RTREE_COORD_T == 4

  CU_ASSERT_EQUAL_FATAL(node_count(root), 2);
  CU_ASSERT_EQUAL_FATAL(node_level(root), 1);

  state_t *state = rtree->state;
  branch_t const *array[2];
  gather_t gather = { .array = array, .count = 2 };
  int err = node_branch_each(state, root, branch_gather_f, &gather);

  CU_ASSERT_EQUAL_FATAL(err, 0);

  const rtree_coord_t
    *rect0 = branch_get_rect(array[0]),
    *rect1 = branch_get_rect(array[1]);

  CU_ASSERT_FALSE(rect_intersect(state, rect0, rect1));

#elif SIZEOF_RTREE_COORD_T == 8

  CU_ASSERT_EQUAL_FATAL(node_count(root), 3);
  CU_ASSERT_EQUAL_FATAL(node_level(root), 1);

  state_t *state = rtree->state;
  branch_t const *array[3];
  gather_t gather = { .array = array, .count = 3 };
  int err = node_branch_each(state, root, branch_gather_f, &gather);

  CU_ASSERT_EQUAL_FATAL(err, 0);

  const rtree_coord_t
    *rect0 = branch_get_rect(array[0]),
    *rect1 = branch_get_rect(array[1]),
    *rect2 = branch_get_rect(array[2]);

  CU_ASSERT_FALSE(rect_intersect(state, rect0, rect1));
  CU_ASSERT_FALSE(rect_intersect(state, rect0, rect2));
  CU_ASSERT_FALSE(rect_intersect(state, rect1, rect2));

#endif

  rtree_destroy(rtree);
}

void test_rtree_split_horizontal(void)
{
  rtree_split_horizontal_flag(RTREE_NODE_PAGE(1) | RTREE_SPLIT_LINEAR);
  rtree_split_horizontal_flag(RTREE_NODE_PAGE(1) | RTREE_SPLIT_QUADRATIC);
  rtree_split_horizontal_flag(RTREE_NODE_PAGE(1) | RTREE_SPLIT_GREENE);
}

void rtree_split_vertical_flag(unsigned flags)
{
  rtree_t *rtree = fixture_csv_rtree(2, flags, "split-vertical.csv");
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(rtree_height(rtree), 2);

  node_t *root = rtree->root;
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

#if SIZEOF_RTREE_COORD_T == 4

  CU_ASSERT_EQUAL_FATAL(node_count(root), 2);
  CU_ASSERT_EQUAL_FATAL(node_level(root), 1);

  state_t *state = rtree->state;
  branch_t const *array[2];
  gather_t gather = { .array = array, .count = 2 };
  int err = node_branch_each(state, root, branch_gather_f, &gather);

  CU_ASSERT_EQUAL_FATAL(err, 0);

  const rtree_coord_t
    *rect0 = branch_get_rect(array[0]),
    *rect1 = branch_get_rect(array[1]);

  CU_ASSERT_FALSE(rect_intersect(state, rect0, rect1));

#elif SIZEOF_RTREE_COORD_T == 8

  CU_ASSERT_EQUAL_FATAL(node_count(root), 3);
  CU_ASSERT_EQUAL_FATAL(node_level(root), 1);

  state_t *state = rtree->state;
  branch_t const *array[3];
  gather_t gather = { .array = array, .count = 3 };
  int err = node_branch_each(state, root, branch_gather_f, &gather);

  CU_ASSERT_EQUAL_FATAL(err, 0);

  const rtree_coord_t
    *rect0 = branch_get_rect(array[0]),
    *rect1 = branch_get_rect(array[1]),
    *rect2 = branch_get_rect(array[2]);

  CU_ASSERT_FALSE(rect_intersect(state, rect0, rect1));
  CU_ASSERT_FALSE(rect_intersect(state, rect0, rect2));
  CU_ASSERT_FALSE(rect_intersect(state, rect1, rect2));

#endif

  rtree_destroy(rtree);
}

void test_rtree_split_vertical(void)
{
  rtree_split_vertical_flag(RTREE_NODE_PAGE(1) | RTREE_SPLIT_LINEAR);
  rtree_split_vertical_flag(RTREE_NODE_PAGE(1) | RTREE_SPLIT_QUADRATIC);
  rtree_split_vertical_flag(RTREE_NODE_PAGE(1) | RTREE_SPLIT_GREENE);
}

/*
  This is the original Guttman-Green test-case, which finds the
  rectangles at offset 1, 2 intersect the target rectangle.  We
  store the offsets as the ids of the rectangles (recall that we
  don't have any problems with zero ids), the the search functions
  stores those in "arg".  We make no assertion about the order,
  but we can assert that the values are 1, 2 by asserting the
  product is 2.
*/

typedef struct
{
  unsigned long found[4];
  size_t count;
} guttman_t;

static int guttman_cb(unsigned long i, void *arg)
{
  guttman_t *g = (guttman_t*)arg;
  g->found[g->count] = i;
  g->count++;

  return 0;
}

void rtree_guttman_flag(unsigned flags)
{
  rtree_coord_t rects[][4] = {
    {0, 0, 2, 2},
    {5, 5, 7, 7},
    {8, 5, 9, 6},
    {7, 1, 9, 2},
  };
  rtree_coord_t target[4] = {6, 4, 10, 6};
  rtree_t *rtree = rtree_new(2, flags);

  for (size_t i = 0 ; i < 4 ; i++)
    CU_ASSERT_EQUAL(rtree_add_rect(rtree, i, rects[i]), 0);

  guttman_t arg = { .found = {0}, .count = 0 };

  CU_ASSERT_EQUAL(rtree_search(rtree, target, guttman_cb, &arg), 0);
  CU_ASSERT_EQUAL(arg.count, 2);
  CU_ASSERT_EQUAL(arg.found[0] * arg.found[1], 2);

  rtree_destroy(rtree);
}

void test_rtree_guttman(void)
{
  rtree_guttman_flag(RTREE_SPLIT_LINEAR);
  rtree_guttman_flag(RTREE_SPLIT_QUADRATIC);
  rtree_guttman_flag(RTREE_SPLIT_GREENE);
}

void test_rtree_bytes_null(void)
{
  CU_ASSERT_EQUAL(rtree_bytes(NULL), 0);
}

void test_rtree_bytes_not_null(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_NOT_EQUAL(rtree_bytes(rtree), 0);
  rtree_destroy(rtree);
}

void test_rtree_dims_null(void)
{
  CU_ASSERT_EQUAL(rtree_dims(NULL), 0);
}

void test_rtree_dims_not_null(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_EQUAL(rtree_dims(rtree), 2);
  rtree_destroy(rtree);
}

void test_rtree_page_size_null(void)
{
  CU_ASSERT_EQUAL(rtree_page_size(NULL), 0);
}

void test_rtree_page_size_not_null(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  size_t page_size = rtree_page_size(rtree);

  CU_ASSERT_NOT_EQUAL(page_size, 0);
  CU_ASSERT_DYADIC_POWER(page_size);

  rtree_destroy(rtree);
}

void test_rtree_node_size_null(void)
{
  CU_ASSERT_EQUAL(rtree_node_size(NULL), 0);
}

void test_rtree_node_size_not_null(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_NOT_EQUAL(rtree_node_size(rtree), 0);
  rtree_destroy(rtree);
}

void test_rtree_rect_size_null(void)
{
  CU_ASSERT_EQUAL(rtree_rect_size(NULL), 0);
}

void test_rtree_rect_size_not_null(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_NOT_EQUAL(rtree_rect_size(rtree), 0);
  rtree_destroy(rtree);
}

void test_rtree_branch_size_null(void)
{
  CU_ASSERT_EQUAL(rtree_branch_size(NULL), 0);
}

void test_rtree_branch_size_not_null(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_NOT_EQUAL(rtree_branch_size(rtree), 0);
  rtree_destroy(rtree);
}

void test_rtree_branching_factor_null(void)
{
  CU_ASSERT_EQUAL(rtree_branching_factor(NULL), 0);
}

void test_rtree_branching_factor_not_null(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_NOT_EQUAL(rtree_branching_factor(rtree), 0);
  rtree_destroy(rtree);
}

void test_rtree_unit_sphere_volume_null(void)
{
  CU_ASSERT_EQUAL(rtree_unit_sphere_volume(NULL), 0);
}

void test_rtree_unit_sphere_volume_not_null(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  rtree_coord_t volume = rtree_unit_sphere_volume(rtree);
  rtree_destroy(rtree);

  rtree_coord_t eps;

  switch (sizeof(rtree_coord_t))
    {
    case 8: eps = DBL_EPSILON; break;
    case 4: eps = FLT_EPSILON; break;
    default:
      CU_FAIL("strange rtree_coord_t size");
    }

  CU_ASSERT_DOUBLE_EQUAL(volume, M_PI, 2 * eps * M_PI);
}

void test_rtree_empty_null(void)
{
  CU_ASSERT_TRUE(rtree_empty(NULL));
}

void test_rtree_empty_not_null(void)
{
  rtree_t *rtree = rtree_new(2, 0);

  CU_ASSERT_TRUE(rtree_empty(rtree));

  rtree_coord_t rect[4] = {0, 0, 1, 1};
  int err = rtree_add_rect(rtree, 43, rect);

  CU_ASSERT_EQUAL(err, 0);
  CU_ASSERT_FALSE(rtree_empty(rtree));

  rtree_destroy(rtree);
}

void test_rtree_empty_split(void)
{
  rtree_t *rtree = fixture_csv_rtree(2, 0, "split-horizontal.csv");
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_FALSE(rtree_empty(rtree));
  rtree_destroy(rtree);
}

void test_rtree_envelope_null(void)
{
  rtree_coord_t env_rect[4];
  int err = rtree_envelope(NULL, env_rect);
  CU_ASSERT_NOT_EQUAL(err, 0);
}

void test_rtree_envelope_empty(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_coord_t env_rect[4];
  int err = rtree_envelope(rtree, env_rect);
  CU_ASSERT_NOT_EQUAL(err, 0);
  rtree_destroy(rtree);
}

void test_rtree_envelope_single(void)
{
  int err;
  double eps = 1e-16;
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_coord_t rect[4] = {1, 1, 2, 3};
  err = rtree_add_rect(rtree, 43, rect);
  CU_ASSERT_EQUAL(err, 0);

  rtree_coord_t env_rect[4];
  err = rtree_envelope(rtree, env_rect);
  CU_ASSERT_EQUAL(err, 0);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[0], 1, eps);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[1], 1, eps);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[2], 2, eps);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[3], 3, eps);
}

void test_rtree_envelope_nested(void)
{
  int err;
  double eps = 1e-16;
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_coord_t rect1[4] = {1, 1, 2, 2};
  err = rtree_add_rect(rtree, 43, rect1);
  CU_ASSERT_EQUAL(err, 0);

  rtree_coord_t rect2[4] = {0, 0, 3, 3};
  err = rtree_add_rect(rtree, 44, rect2);
  CU_ASSERT_EQUAL(err, 0);

  rtree_coord_t env_rect[4];
  err = rtree_envelope(rtree, env_rect);
  CU_ASSERT_EQUAL(err, 0);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[0], 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[1], 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[2], 3, eps);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[3], 3, eps);
}

void test_rtree_envelope_cross(void)
{
  int err;
  double eps = 1e-16;
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_coord_t rect1[4] = {1, 0, 2, 3};
  err = rtree_add_rect(rtree, 43, rect1);
  CU_ASSERT_EQUAL(err, 0);

  rtree_coord_t rect2[4] = {0, 1, 3, 2};
  err = rtree_add_rect(rtree, 44, rect2);
  CU_ASSERT_EQUAL(err, 0);

  rtree_coord_t env_rect[4];
  err = rtree_envelope(rtree, env_rect);
  CU_ASSERT_EQUAL(err, 0);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[0], 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[1], 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[2], 3, eps);
  CU_ASSERT_DOUBLE_EQUAL(env_rect[3], 3, eps);
}
