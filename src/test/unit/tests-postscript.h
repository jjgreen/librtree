#include <CUnit/CUnit.h>

extern CU_TestInfo tests_postscript[];

void test_postscript_null(void);
void test_postscript_bad_dim(void);
void test_postscript_empty(void);
void test_postscript_nonempty(void);
void test_postscript_split(void);
void test_postscript_under_styled(void);
void test_postscript_style_valid(void);
void test_postscript_style_absent_fill(void);
void test_postscript_style_absent_stroke(void);
void test_postscript_style_invalid_key(void);
void test_postscript_style_invalid_grey(void);
void test_postscript_bbox_shift(void);
void test_postscript_bbox_width(void);
void test_postscript_bbox_height(void);
