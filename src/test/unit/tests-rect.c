#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-rect.h"

#include <private/rect.h>
#include <private/state.h>

CU_TestInfo tests_rect[] = {
  {"init", test_rect_init},
  {"intersect", test_rect_intersect},
  {"volume, dim 1", test_rect_volume_dim1},
  {"volume, dim 2", test_rect_volume_dim2},
  {"volume, dim 3", test_rect_volume_dim3},
  {"spherical volume, dim 1", test_rect_spherical_volume_dim1},
  {"spherical volume, dim 2", test_rect_spherical_volume_dim2},
  {"spherical volume, dim 3", test_rect_spherical_volume_dim3},
  {"combine, simple, dim 2", test_rect_combine_simple_dim2},
  {"combine, simple, dim 3", test_rect_combine_simple_dim3},
  {"combine, simple, dim 4", test_rect_combine_simple_dim4},
  {"combine, simple, dim 6", test_rect_combine_simple_dim6},
  {"merge", test_rect_merge},
  {"copy", test_rect_copy},
  {"identical", test_rect_identical},
  {"alloc", test_rects_alloc},
  CU_TEST_INFO_NULL
};

void test_rect_init(void)
{
  const size_t dims = 2;
  state_t *state = state_new(dims, 0);
  rtree_coord_t rect[2 * dims];

  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  CU_ASSERT_EQUAL(rect_init(state, rect), 0);

  for (size_t i = 0 ; i < 2 * dims ; i++)
    CU_ASSERT_EQUAL(rect[i], 0);

  state_destroy(state);
}

void test_rect_intersect(void)
{
  const size_t dims = 2;
  state_t *state = state_new(dims, 0);

  {
    const rtree_coord_t
      a[] = {0, 0, 1, 1},
      b[] = {0, 2, 1, 3};
    CU_ASSERT_FALSE(rect_intersect(state, a, b));
  }

  {
    const rtree_coord_t
      a[] = {0, 0, 1, 1},
      b[] = {2, 0, 3, 1};
    CU_ASSERT_FALSE(rect_intersect(state, a, b));
  }

  {
    const rtree_coord_t
      a[] = {0, 0, 1, 1},
      b[] = {0, 1, 1, 2};
    CU_ASSERT_TRUE(rect_intersect(state, a, b));
  }

  {
    const rtree_coord_t
      a[] = {0, 0, 1, 1},
      b[] = {0, 0.5, 1, 1.5};
    CU_ASSERT_TRUE(rect_intersect(state, a, b));
  }

  {
    const rtree_coord_t
      a[] = {0, 0, 3, 3},
      b[] = {1, 1, 2, 2};
    CU_ASSERT_TRUE(rect_intersect(state, a, b));
  }

  state_destroy(state);
}

void test_rect_volume_dim1(void)
{
  const size_t dims = 1;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t rect[] = { 0.5, 1 };
  rtree_coord_t vol = rect_volume(state, rect);

  CU_ASSERT_DOUBLE_EQUAL(vol, 0.5, 1e-7);

  state_destroy(state);
}

void test_rect_volume_dim2(void)
{
  const size_t dims = 2;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t rect[] = { 0, 0.5, 1, 1 };
  rtree_coord_t vol = rect_volume(state, rect);

  CU_ASSERT_DOUBLE_EQUAL(vol, 0.5, 1e-7);

  state_destroy(state);
}

void test_rect_volume_dim3(void)
{
  const size_t dims = 3;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t rect[] = { 0, 0.5, 0.75, 1, 1, 1};
  rtree_coord_t vol = rect_volume(state, rect);

  CU_ASSERT_DOUBLE_EQUAL(vol, 0.125, 1e-6);

  state_destroy(state);
}

void test_rect_spherical_volume_dim1(void)
{
  const size_t dims = 1;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t rect[] = { 0, 1 };
  rtree_coord_t vol = rect_spherical_volume(state, rect);

  CU_ASSERT_DOUBLE_EQUAL(vol, 1.0, 1e-7);

  state_destroy(state);
}

void test_rect_spherical_volume_dim2(void)
{
  const size_t dims = 2;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t rect[] = { 0, 0, 1, 1 };
  rtree_coord_t vol = rect_spherical_volume(state, rect);

  CU_ASSERT_DOUBLE_EQUAL(vol, M_PI / 2.0, 1e-7);

  state_destroy(state);
}

void test_rect_spherical_volume_dim3(void)
{
  const size_t dims = 3;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t rect[] = { 0, 0, 0, 1, 1, 1};
  rtree_coord_t vol = rect_spherical_volume(state, rect);

  CU_ASSERT_DOUBLE_EQUAL(vol, M_PI * sqrt(3) / 2.0, 1e-6);

  state_destroy(state);
}

void test_rect_combine_simple_dim2(void)
{
  const size_t dims = 2;
  state_t *state = state_new(dims, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t
    r0[] = {1, 0, 3, 0},
    r1[] = {0, 0, 0, 2},
    rx[] = {0, 0, 3, 2};
  rtree_coord_t
    r2[2 * dims];

  rect_combine(state, r0, r1, r2);

  for (size_t i = 0 ; i < 2 * dims ; i++)
    CU_ASSERT_EQUAL(r2[i], rx[i]);

  state_destroy(state);
}

void test_rect_combine_simple_dim3(void)
{
  const size_t dims = 3;
  state_t *state = state_new(dims, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t
    r0[] = {1, 0, 1, 3, 2, 3},
    r1[] = {0, 1, 0, 2, 3, 2},
    rx[] = {0, 0, 0, 3, 3, 3};
  rtree_coord_t
    r2[2 * dims];

  rect_combine(state, r0, r1, r2);

  for (size_t i = 0 ; i < 2 * dims ; i++)
    CU_ASSERT_EQUAL(r2[i], rx[i]);

  state_destroy(state);
}

void test_rect_combine_simple_dim4(void)
{
  const size_t dims = 4;
  state_t *state = state_new(dims, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t
    r0[] = {1, 0, 1, 0, 3, 2, 3, 2},
    r1[] = {0, 1, 0, 1, 2, 3, 2, 3},
    rx[] = {0, 0, 0, 0, 3, 3, 3, 3};
  rtree_coord_t
    r2[2 * dims];

  rect_combine(state, r0, r1, r2);

  for (size_t i = 0 ; i < 2 * dims ; i++)
    CU_ASSERT_EQUAL(r2[i], rx[i]);

  state_destroy(state);
}

void test_rect_combine_simple_dim6(void)
{
  const size_t dims = 6;
  state_t *state = state_new(dims, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t
    r0[] = {1, 0, 1, 0, 1, 0, 3, 2, 3, 2, 3, 2},
    r1[] = {0, 1, 0, 1, 0, 1, 2, 3, 2, 3, 2, 3},
    rx[] = {0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3};
  rtree_coord_t
    r2[2 * dims];

  rect_combine(state, r0, r1, r2);

  for (size_t i = 0 ; i < 2 * dims ; i++)
    CU_ASSERT_EQUAL(r2[i], rx[i]);

  state_destroy(state);
}

void test_rect_merge(void)
{
  const size_t dims = 2;
  state_t *state = state_new(dims, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t r0[] = {0, 0, 2, 3};
  rtree_coord_t r1[] = {1, 1, 3, 4};

  rect_merge(state, r0, r1);

  CU_ASSERT_EQUAL(r1[0], 0);
  CU_ASSERT_EQUAL(r1[1], 0);
  CU_ASSERT_EQUAL(r1[2], 3);
  CU_ASSERT_EQUAL(r1[3], 4);

  state_destroy(state);
}

void test_rect_copy(void)
{
  for (size_t dims = 2 ; dims < 10 ; dims++)
    {
      size_t n = 2 * dims;
      state_t *state = state_new(dims, 0);

      CU_ASSERT_PTR_NOT_NULL_FATAL(state);

      rtree_coord_t src[n], dest[n];

      for (size_t i = 0 ; i < n ; i++)
        {
          src[i] = i;
          dest[i] = 0;
        }

      rect_copy(state, src, dest);

      for (size_t i = 0 ; i < 4 ; i++)
        CU_ASSERT_EQUAL(dest[i], i);

      state_destroy(state);
    }
}

void test_rect_identical(void)
{
  const size_t dims = 2;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const rtree_coord_t
    r0[] = {1, 0, 3, 2},
    r1[] = {1, 0, 3, 2},
    r2[] = {0, 0, 3, 2};

  CU_ASSERT_TRUE(rect_identical(state, r0, r0));
  CU_ASSERT_TRUE(rect_identical(state, r0, r1));
  CU_ASSERT_FALSE(rect_identical(state, r0, r2));

  state_destroy(state);
}

void test_rects_alloc(void)
{
  const size_t dims = 2;
  state_t *state = state_new(dims, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  const size_t n = 10;
  rtree_coord_t *rects[n];

  CU_ASSERT_EQUAL_FATAL(rects_alloc(state, n, rects), 0);

  for (size_t i = 0 ; i < n ; i++)
    CU_ASSERT_EQUAL(rect_init(state, rects[i]), 0);

  rects_free(n, rects);
  state_destroy(state);
}
