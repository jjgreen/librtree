#include <CUnit/CUnit.h>

extern CU_TestInfo tests_rect[];

void test_rect_init(void);
void test_rect_volume_dim1(void);
void test_rect_volume_dim2(void);
void test_rect_volume_dim3(void);
void test_rect_spherical_volume_dim1(void);
void test_rect_spherical_volume_dim2(void);
void test_rect_spherical_volume_dim3(void);
void test_rect_intersect(void);
void test_rect_combine_simple_dim2(void);
void test_rect_combine_simple_dim3(void);
void test_rect_combine_simple_dim4(void);
void test_rect_combine_simple_dim6(void);
void test_rect_merge(void);
void test_rect_copy(void);
void test_rect_identical(void);
void test_rects_alloc(void);
