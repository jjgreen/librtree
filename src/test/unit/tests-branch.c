#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-branch.h"

#include <private/branch.h>
#include <private/state.h>

CU_TestInfo tests_branch[] = {
  {"sizeof positive", test_branch_sizeof_positive},
  {"sizeof nondecreasing", test_branch_sizeof_nondecreasing},
  {"init", test_branch_init},
  CU_TEST_INFO_NULL
};

void test_branch_sizeof_positive(void)
{
  for (size_t n = 1 ; n < 10 ; n++)
    CU_ASSERT(branch_sizeof(n) > 0);
}

void test_branch_sizeof_nondecreasing(void)
{
  for (size_t n = 1 ; n < 10 ; n++)
    CU_ASSERT(branch_sizeof(n) <= branch_sizeof(n + 1));
}

void test_branch_init(void)
{
  size_t dims = 2;
  state_t *state = state_new(dims, 0);
  size_t branch_size = state_branch_size(state);
  char bytes[branch_size];
  branch_t *branch = (branch_t*)bytes;

  CU_ASSERT_EQUAL(branch_init(state, branch), 0);
  CU_ASSERT_PTR_NULL(branch_get_child(branch));

  state_destroy(state);
}
