#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "assert-dyadic-power.h"

bool dyadic_power(size_t n)
{
  if (n == 0)
    return false;

  while (n != 1)
    {
      if ((n % 2) != 0)
        return false;
      n /= 2;
    }

  return true;
}
