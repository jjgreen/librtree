#include <CUnit/CUnit.h>

extern CU_TestInfo tests_node[];

void test_node_init(void);
void test_node_new(void);
void test_node_branch_each(void);
void test_node_add_branch(void);
void test_node_detach_branch(void);
void test_node_envelope_empty(void);
void test_node_envelope_single(void);
void test_node_envelope_multiple(void);
void test_node_add_rect_single(void);
void test_node_add_rect_no_split(void);
void test_node_add_rect_split(void);
void test_node_clone_empty(void);
void test_node_identical_null(void);
void test_node_identical_empty(void);
void test_node_identical_nonempty(void);
void test_node_height_empty(void);
void test_node_height_single(void);
void test_node_height_split(void);
void test_node_bytes_null(void);
void test_node_bytes_single(void);
