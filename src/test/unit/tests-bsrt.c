#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-bsrt.h"

#include "fixture-bsrt.h"
#include "fixture-csv.h"

#include <private/bsrt.h>

#ifdef WITH_BSRT

CU_TestInfo tests_bsrt[] = {
  {"write empty", test_bsrt_write_empty},
  {"write non-empty", test_bsrt_write_nonempty},
  {"read bad-magic", test_bsrt_read_bad_magic},
  {"read bad-version", test_bsrt_read_bad_version},
  {"read bad-header", test_bsrt_read_bad_header},
  {"read fuzz-01", test_bsrt_read_fuzz01},
  {"read fuzz-02", test_bsrt_read_fuzz02},
  {"read v1 empty", test_bsrt_read_v1_empty},
  {"read v1 non-empty", test_bsrt_read_v1_nonempty},
  {"read v2 empty", test_bsrt_read_v2_empty},
  {"read v2 non-empty", test_bsrt_read_v2_nonempty},
  {"reflexive", test_bsrt_reflexive},
  CU_TEST_INFO_NULL
};

/* check bsrt stream against a fixture */

static void assert_matches_stream(FILE *st1, FILE *st2)
{
  int c1, c2;

  do {
    c1 = getc(st1);
    c2 = getc(st2);

    if (c1 != c2)
      {
        CU_FAIL("stream mismatch");
        return;
      }
  } while (c1 != EOF);

  CU_PASS("streams match");
}

static void assert_matches_fixture(FILE *reference_stream, const char *name)
{
  FILE *fixture_stream = fixture_bsrt_stream(name);
  CU_ASSERT_PTR_NOT_NULL_FATAL(fixture_stream);
  rewind(reference_stream);
  assert_matches_stream(reference_stream, fixture_stream);
  fclose(fixture_stream);
}

#define CU_ASSERT_MATCHES_FIXTURE(stream, name) \
  assert_matches_fixture(stream, name)

void test_bsrt_write_empty(void)
{
  FILE *stream = tmpfile();
  rtree_t *rtree = rtree_new(2, RTREE_NODE_PAGE(1));

  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(bsrt_rtree_write(rtree, stream), 0);
  CU_ASSERT_MATCHES_FIXTURE(stream, "2/empty.bsrt");

  rtree_destroy(rtree);
  fclose(stream);
}

void test_bsrt_write_nonempty(void)
{
  FILE *stream = tmpfile();
  size_t dims = 2;
  rtree_coord_t rect[] = {
    0.0, 1.0,
    2.0, 4.0
  };
  rtree_t *rtree = rtree_new(dims, RTREE_NODE_PAGE(1));

  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(rtree_add_rect(rtree, 0xefbeadde, rect), 0);
  CU_ASSERT_EQUAL(bsrt_rtree_write(rtree, stream), 0);
  CU_ASSERT_MATCHES_FIXTURE(stream, "2/nonempty.bsrt");

  rtree_destroy(rtree);
  fclose(stream);
}

void test_bsrt_read_bad_magic(void)
{
  FILE *stream = fixture_bsrt_stream("bad-magic.bsrt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  errno = 0;
  rtree_t *rtree = bsrt_rtree_read(stream);
  CU_ASSERT_PTR_NULL(rtree);
  CU_ASSERT_EQUAL(errno, EINVAL);

  fclose(stream);
}

void test_bsrt_read_bad_version(void)
{
  FILE *stream = fixture_bsrt_stream("bad-version.bsrt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  errno = 0;
  rtree_t *rtree = bsrt_rtree_read(stream);
  CU_ASSERT_PTR_NULL(rtree);
  CU_ASSERT_EQUAL(errno, EINVAL);

  fclose(stream);
}

void test_bsrt_read_bad_header(void)
{
  FILE *stream = fixture_bsrt_stream("bad-header.bsrt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  errno = 0;
  rtree_t *rtree = bsrt_rtree_read(stream);
  CU_ASSERT_PTR_NULL(rtree);
  CU_ASSERT_EQUAL(errno, EINVAL);

  fclose(stream);
}

void test_bsrt_read_fuzz01(void)
{
  FILE *stream = fixture_bsrt_stream("afl-01.bsrt");
  if (stream != NULL)
    {
      errno = 0;
      rtree_t *rtree = bsrt_rtree_read(stream);
      CU_ASSERT_PTR_NULL(rtree);
      CU_ASSERT_EQUAL(errno, EINVAL);
      fclose(stream);
    }
}

void test_bsrt_read_fuzz02(void)
{
  FILE *stream = fixture_bsrt_stream("afl-02.bsrt");
  if (stream != NULL)
    {
      errno = 0;
      rtree_t *rtree = bsrt_rtree_read(stream);
      CU_ASSERT_PTR_NULL(rtree);
      CU_ASSERT_EQUAL(errno, EINVAL);
      fclose(stream);
    }
}

void test_bsrt_read_v1_empty(void)
{
  FILE *stream = fixture_bsrt_stream("1/empty.bsrt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = bsrt_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_destroy(rtree);
  fclose(stream);
}

void test_bsrt_read_v1_nonempty(void)
{
  FILE *stream = fixture_bsrt_stream("1/nonempty.bsrt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = bsrt_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_destroy(rtree);
  fclose(stream);
}

void test_bsrt_read_v2_empty(void)
{
  FILE *stream = fixture_bsrt_stream("2/empty.bsrt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = bsrt_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_destroy(rtree);
  fclose(stream);
}

void test_bsrt_read_v2_nonempty(void)
{
  FILE *stream = fixture_bsrt_stream("2/nonempty.bsrt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = bsrt_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_destroy(rtree);
  fclose(stream);
}

/*
  These test for reflexivity, if we write a tree, then read it,
  do we end up with the same tree?
*/

static void reflexive(size_t dims, const char *file)
{
  rtree_t *rt1 = fixture_csv_rtree(dims, 0, file);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rt1);

  FILE *stream = tmpfile();
  CU_ASSERT_EQUAL(bsrt_rtree_write(rt1, stream), 0);
  rewind(stream);
  rtree_t *rt2 = bsrt_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rt2);
  fclose(stream);

  CU_ASSERT_TRUE(rtree_identical(rt1, rt2));

  rtree_destroy(rt1);
  rtree_destroy(rt2);
}

void test_bsrt_reflexive(void)
{
  reflexive(2, "single-square.csv");
  reflexive(2, "zero-id.csv");
  reflexive(2, "split-horizontal.csv");
  reflexive(2, "split-vertical.csv");
  reflexive(2, "split-square.csv");
}

#else

CU_TestInfo tests_bsrt[] = {
  {"write no-bsrt", test_bsrt_write_nobsrt},
  {"read no-bsrt", test_bsrt_read_nobsrt},
  CU_TEST_INFO_NULL
};

void test_bsrt_write_nobsrt(void)
{
  FILE *stream = tmpfile();
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  errno = 0;
  CU_ASSERT_EQUAL(bsrt_rtree_write(rtree, stream), RTREE_ERR_NOBSRT);
  CU_ASSERT_EQUAL(errno, ENOSYS);

  fclose(stream);
  rtree_destroy(rtree);
}

void test_bsrt_read_nobsrt(void)
{
  FILE *stream = tmpfile();
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  errno = 0;
  rtree_t *rtree = bsrt_rtree_read(stream);
  CU_ASSERT_PTR_NULL(rtree);
  CU_ASSERT_EQUAL(errno, ENOSYS);

  fclose(stream);
}

#endif
