#include <CUnit/CUnit.h>

extern CU_TestInfo tests_branch[];

void test_branch_sizeof_positive(void);
void test_branch_sizeof_nondecreasing(void);
void test_branch_init(void);
void test_branch_leaf_flag(void);
