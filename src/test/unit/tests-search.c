#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-search.h"
#include "fixture-csv.h"

#include <private/search.h>
#include <private/rtree.h>

#include <stdlib.h>

CU_TestInfo tests_search[] = {
  {"empty", test_search_empty},
  {"single-square", test_search_single_square},
  {"single-line", test_search_single_line},
  {"split horizontal", test_search_split_horizontal},
  {"regular grid small", test_search_grid_small},
  {"regular grid large", test_search_grid_large},
  CU_TEST_INFO_NULL
};

/*
  callbacks passed to search(), it just increments the void*
  argument (which should be a size_t*) so counts the number
  of intersecting rectangles in the tree.
*/

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
static int count(rtree_id_t id, void *arg)
{
  (*((size_t*)arg))++;
  return 0;
}
#pragma GCC diagnostic pop

void test_search_empty(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_coord_t rect[] = {0, 0, 1, 1};
  size_t n = 0;
  int err = search(rtree->state, rect, rtree->root, count, &n);

  CU_ASSERT_EQUAL(err, 0);
  CU_ASSERT_EQUAL(n, 0);

  rtree_destroy(rtree);
}

/*
  Fixture is a single square 1 ≤ x ≤ 2, 2 ≤ y ≤ 3, in an R-tree,
  we test whether various rectangles are correctly reported as
  intersecting (or not) this tree -- an important case is where
  the query rectangle is positive codimension, i.e., is a line
  segment or a point (a degenerate rectangle).
*/

static void search_single_square(unsigned flags)
{
  rtree_t *rtree = fixture_csv_rtree(2, flags, "single-square.csv");
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  /* disjoint tests */

  {
    rtree_coord_t rect[][4] =
      {
       {2.5, 3.5, 3.5, 4.5}
      };
    size_t k = sizeof(rect) / (4 * sizeof(rtree_coord_t));

    for (size_t i = 0 ; i < k ; i++)
      {
        size_t n = 0;
        int err = search(rtree->state, rect[i], rtree->root, count, &n);

        CU_ASSERT_EQUAL(err, 0);
        CU_ASSERT_EQUAL(n, 0);
      }
  }

  /* intersecting cases */

  {
    rtree_coord_t rect[][4] =
      {
       /* proper rectangles */

       {1.3, 2.3, 1.7, 2.7}, /* inside */
       {1.5, 2.5, 2.5, 3.5}, /* intersecting */
       {0.0, 1.0, 3.0, 4.0}, /* containing */

       /* vertical line */

       {1.5, 1, 1.5, 4}, /* puncturing */
       {1.5, 1, 1.5, 2}, /* touching bottom */
       {1.5, 3, 1.5, 4}, /* touching top */

       /* horizontal line */

       {0, 2.5, 3, 2.5}, /* puncturing */
       {0, 2.5, 1, 2.5}, /* touching left */
       {2, 2.5, 3, 2.5}, /* touching right */

       /* point */

       {1, 2, 1, 2}, /* bottom-left corner */
       {2, 2, 2, 2}, /* bottom-right corner */
       {1, 3, 1, 3}, /* top-left corner */
       {2, 3, 2, 3}, /* top-right corner */

      };
    size_t k = sizeof(rect) / (4 * sizeof(rtree_coord_t));

    for (size_t i = 0 ; i < k ; i++)
      {
        size_t n = 0;
        int err = search(rtree->state, rect[i], rtree->root, count, &n);

        CU_ASSERT_EQUAL(err, 0);
        CU_ASSERT_EQUAL(n, 1);
      }
  }

  rtree_destroy(rtree);
}

void test_search_single_square(void)
{
  search_single_square(RTREE_SPLIT_LINEAR);
  search_single_square(RTREE_SPLIT_QUADRATIC);
  search_single_square(RTREE_SPLIT_GREENE);
}

/* Fixture is a single (horizontal) line 1 ≤ x ≤ 2, y = 1 */

static void search_single_line(unsigned flags)
{
  rtree_t *rtree = fixture_csv_rtree(2, flags, "single-line.csv");
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  /* disjoint tests */

  {
    rtree_coord_t rect[][4] =
      {
       {2, 2, 3, 3}
      };
    size_t k = sizeof(rect) / (4 * sizeof(rtree_coord_t));

    for (size_t i = 0 ; i < k ; i++)
      {
        size_t n = 0;
        int err = search(rtree->state, rect[i], rtree->root, count, &n);

        CU_ASSERT_EQUAL(err, 0);
        CU_ASSERT_EQUAL(n, 0);
      }
  }

  /* intersecting cases */

  {
    rtree_coord_t rect[][4] =
      {
       /* proper rectangle */

       {0, 0, 3, 2}, /* containing */
       {1, 1, 2, 2}, /* touching above */
       {1, 0, 2, 1}, /* touching below */
       {0, 0, 1, 2}, /* touching left */
       {2, 0, 3, 2}, /* touching right */

       /* horizontal line */

       {0, 1, 3, 1}, /* containing */
       {0, 1, 1, 1}, /* touching left */
       {2, 1, 3, 1}, /* touching right */

       /* vertical line */

       {1, 0, 1, 2}, /* touching left */
       {2, 0, 2, 2}, /* touching right */

       /* point */

       {1, 1, 1, 1}, /* left endpoint */
       {2, 1, 2, 1}, /* right endpoint */

      };
    size_t k = sizeof(rect) / (4 * sizeof(rtree_coord_t));

    for (size_t i = 0 ; i < k ; i++)
      {
        size_t n = 0;
        int err = search(rtree->state, rect[i], rtree->root, count, &n);

        CU_ASSERT_EQUAL(err, 0);
        CU_ASSERT_EQUAL(n, 1);
      }
  }

  rtree_destroy(rtree);
}

void test_search_single_line(void)
{
  search_single_line(RTREE_SPLIT_LINEAR);
  search_single_line(RTREE_SPLIT_QUADRATIC);
  search_single_line(RTREE_SPLIT_GREENE);
}

static void search_split_horizontal(unsigned flags)
{
  rtree_t *rtree = fixture_csv_rtree(2, flags, "split-horizontal.csv");
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  {
    /* inside the first square */

    rtree_coord_t rect[] = {0.2, 0.25, 0.3, 0.75};
    size_t n = 0;
    int err = search(rtree->state, rect, rtree->root, count, &n);

    CU_ASSERT_EQUAL(err, 0);
    CU_ASSERT_EQUAL(n, 1);
  }

  {
    /* intersecting the first 3 squares */

    rtree_coord_t rect[] = {0.2, 0.25, 2.3, 0.75};
    size_t n = 0;
    int err = search(rtree->state, rect, rtree->root, count, &n);

    CU_ASSERT_EQUAL(err, 0);
    CU_ASSERT_EQUAL(n, 3);
  }

  {
    /* intersecting all squares */

    rtree_coord_t rect[] = {0.2, 0.25, 128.3, 0.75};
    size_t n = 0;
    int err = search(rtree->state, rect, rtree->root, count, &n);

    CU_ASSERT_EQUAL(err, 0);
    CU_ASSERT_EQUAL(n, 129);
  }

  {
    /* outside all squares but inside the envelope */

    rtree_coord_t rect[] = {0.7, 0.25, 0.8, 0.75};
    size_t n = 0;
    int err = search(rtree->state, rect, rtree->root, count, &n);

    CU_ASSERT_EQUAL(err, 0);
    CU_ASSERT_EQUAL(n, 0);
  }

  {
    /* outside the envelope */

    rtree_coord_t rect[] = {0.7, 1.25, 0.8, 1.75};
    size_t n = 0;
    int err = search(rtree->state, rect, rtree->root, count, &n);

    CU_ASSERT_EQUAL(err, 0);
    CU_ASSERT_EQUAL(n, 0);
  }

  rtree_destroy(rtree);
}

void test_search_split_horizontal(void)
{
  search_split_horizontal(RTREE_SPLIT_LINEAR);
  search_split_horizontal(RTREE_SPLIT_QUADRATIC);
  search_split_horizontal(RTREE_SPLIT_GREENE);
}

/*
  regular grid tests, the input to the tree is a uniform grid
  of squares, so we can work out what the search results should
  be quite easily, we then compare that with what we get.
*/

typedef struct
{
  size_t n;
  rtree_id_t *id;
} results_t;

static int gather(rtree_id_t id, void *arg)
{
  results_t *results = (results_t*)arg;
  results->id[results->n] = id;
  results->n++;
  return 0;
}

static rtree_t* regular_grid(unsigned n, unsigned m)
{
  rtree_t *rtree = rtree_new(2, 0);
  if (rtree == NULL)
    return NULL;

  for (unsigned i = 0 ; i < n ; i++)
    {
      for (unsigned j = 0 ; j < m ; j++)
        {
          rtree_coord_t rect[4] = { i, j, i + 1, j + 1 };
          if (rtree_add_rect(rtree, i + j * n, rect) != 0)
            return NULL;
        }
    }

  return rtree;
}

static int id_compare(const void *va, const void *vb)
{
  rtree_id_t
    a = *(rtree_id_t*)va,
    b = *(rtree_id_t*)vb;
  if (a > b)
    return 1;
  else if (a < b)
    return -1;
  else
    return 0;
}

static results_t* rtree_results(rtree_t *rtree, rtree_coord_t rect[4])
{
  size_t n = 0;
  if (search(rtree->state, rect, rtree->root, count, &n) == 0)
    {
      results_t *results;
      if ((results = malloc(sizeof(results_t))) != NULL)
        {
          rtree_id_t *id;
          if ((id = malloc(n * SIZEOF_RTREE_ID_T)) != NULL)
            {
              results->n = 0;
              results->id = id;
              if (search(rtree->state, rect, rtree->root, gather, results) == 0)
                {
                  qsort(id, n, SIZEOF_RTREE_ID_T, id_compare);
                  return results;
                }
            }
        }
    }
  return NULL;
}

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

static results_t* grid_results(size_t n, size_t m, rtree_coord_t rect[4])
{
  size_t
    xmin = floor(MAX(rect[0], 0)),
    xmax = floor(MIN(rect[2], n - 1)),
    ymin = floor(MAX(rect[1], 0)),
    ymax = floor(MIN(rect[3], m - 1)),
    k = (xmax - xmin + 1) * (ymax - ymin + 1);

  results_t *results;
  if ((results = malloc(sizeof(results_t))) != NULL)
    {
      results->n = k;
      if ((results->id = malloc(k * SIZEOF_RTREE_ID_T)) != NULL)
        {
          size_t p = 0;
          for (size_t i = xmin ; i <= xmax ; i++)
            {
              for (size_t j = ymin ; j <= ymax ; j++)
                results->id[p++] = i + j * n;
            }
          qsort(results->id, k, SIZEOF_RTREE_ID_T, id_compare);
        }
    }

  return results;
}

static void results_destroy(results_t *results)
{
  free(results->id);
  free(results);
}

static void assert_results(results_t *results, results_t *expected)
{
  CU_ASSERT_PTR_NOT_NULL_FATAL(results);
  CU_ASSERT_EQUAL(results->n, expected->n);
  for (size_t i = 0 ; i < results->n ; i++)
    CU_ASSERT_EQUAL(results->id[i], expected->id[i]);
}

#define CU_ASSERT_RESULTS(results, expected) assert_results(results, expected)

void test_search_grid_small(void)
{
  size_t n = 4, m = 3;
  rtree_t *rtree = regular_grid(n, m);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_coord_t rect[4] = { 1.25, 0.5, 1.75, 1.5 };
  results_t
    *results = rtree_results(rtree, rect),
    *expected = grid_results(n, m, rect);

  CU_ASSERT_RESULTS(results, expected);

  results_destroy(results);
  results_destroy(expected);
  rtree_destroy(rtree);
}

rtree_coord_t rand_unit(void)
{
  return ((double)rand()) / RAND_MAX;
}

void test_search_grid_large(void)
{
  size_t
    n = 160,
    m = 160;
  rtree_t *rtree = regular_grid(n, m);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  srand(42);

  for (size_t i = 0 ; i < 1024 ; i++)
    {
      rtree_coord_t
        xmin = n * rand_unit(),
        ymin = m * rand_unit(),
        dx = 6 * rand_unit(),
        dy = 6 * rand_unit(),
        rect[] = { xmin, ymin, xmin + dx, ymin + dy };

      results_t
        *results = rtree_results(rtree, rect),
        *expected = grid_results(n, m, rect);

      CU_ASSERT_RESULTS(results, expected);

      results_destroy(results);
      results_destroy(expected);
    }

  rtree_destroy(rtree);
}
