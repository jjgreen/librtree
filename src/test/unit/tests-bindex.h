#include <CUnit/CUnit.h>

extern CU_TestInfo tests_bindex[];

void test_bindex_new(void);
void test_bindex_set_domain(void);
void test_bindex_set_range(void);
void test_bindex_set_valid(void);
void test_bindex_get_domain(void);
void test_bindex_get_unset(void);
void test_bindex_get_set(void);

void test_bindex_first_unset_none(void);
void test_bindex_first_unset_word1(void);
void test_bindex_first_unset_word2(void);
void test_bindex_first_unset_all(void);

void test_bindex_next_unset_none(void);
void test_bindex_next_unset_word1(void);
void test_bindex_next_unset_word2(void);
void test_bindex_next_unset_all(void);
