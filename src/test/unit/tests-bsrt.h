#include <CUnit/CUnit.h>

extern CU_TestInfo tests_bsrt[];

void test_bsrt_write_empty(void);
void test_bsrt_write_nonempty(void);
void test_bsrt_read_bad_magic(void);
void test_bsrt_read_bad_version(void);
void test_bsrt_read_bad_header(void);
void test_bsrt_read_fuzz01(void);
void test_bsrt_read_fuzz02(void);
void test_bsrt_read_v1_empty(void);
void test_bsrt_read_v1_nonempty(void);
void test_bsrt_read_v2_empty(void);
void test_bsrt_read_v2_nonempty(void);
void test_bsrt_reflexive(void);

void test_bsrt_write_nobsrt(void);
void test_bsrt_read_nobsrt(void);
