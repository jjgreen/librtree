#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-state.h"
#include "assert-dyadic-power.h"

#include <private/state.h>
#include <private/rect.h>
#include <private/constants.h>

#include <errno.h>
#include <float.h>

CU_TestInfo tests_state[] = {
  {"new, valid", test_state_new_valid},
  {"new, invalid", test_state_new_invalid},
  {"new, dims too large", test_state_new_dims_too_large},
  {"dims", test_state_dims},
  {"branching factor", test_state_branching_factor},
  {"branch size", test_state_branch_size},
  {"page size", test_state_page_size},
  {"node size", test_state_node_size},
  {"rect size", test_state_rect_size},
  {"unit sphere volume", test_state_unit_sphere_volume},
  {"split", test_state_split},
  {"node-page", test_state_node_page},
  {"identical, nulls", test_state_identical_null},
  {"identical, equal dimension", test_state_identical_dims_equal},
  {"identical, unequal dimension", test_state_identical_dims_unequal},
  {"bytes, NULL", test_state_bytes_null},
  {"bytes, not-NULL", test_state_bytes_non_null},
  CU_TEST_INFO_NULL
};

void test_state_new_valid(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  state_destroy(state);
}

void test_state_new_invalid(void)
{
  errno = 0;
  state_t *state = state_new(0, 0);
  CU_ASSERT_PTR_NULL(state);
  CU_ASSERT_EQUAL(errno, EDOM);
}

void test_state_new_dims_too_large(void)
{
  errno = 0;
  state_t *state = state_new(256, 0);
  CU_ASSERT_PTR_NULL(state);
  CU_ASSERT_EQUAL(errno, EINVAL);
}

void test_state_dims(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  CU_ASSERT_EQUAL(state_dims(state), 2);
  state_destroy(state);
}

void test_state_branching_factor(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  CU_ASSERT(state_branching_factor(state) > 1);
  state_destroy(state);
}

void test_state_page_size(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  size_t page_size = state_page_size(state);
  CU_ASSERT_NOT_EQUAL(page_size, 0);
  CU_ASSERT_DYADIC_POWER(page_size);
  state_destroy(state);
}

void test_state_node_size(void)
{
  size_t n1, n2;

  {
    state_t *state = state_new(2, RTREE_NODE_PAGE(1));
    CU_ASSERT_PTR_NOT_NULL_FATAL(state);
    n1 = state_node_size(state);
    CU_ASSERT(n1 > 0);
    state_destroy(state);
  }

  {
    state_t *state = state_new(2, RTREE_NODE_PAGE(2));
    CU_ASSERT_PTR_NOT_NULL_FATAL(state);
    n2 = state_node_size(state);
    CU_ASSERT(n2 > 0);
    state_destroy(state);
  }

  CU_ASSERT(n1 > n2);
}

void test_state_branch_size(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  CU_ASSERT(state_branch_size(state) > 0);
  state_destroy(state);
}

void test_state_rect_size(void)
{
  state_t *state = state_new(2, 0);
  size_t float_size = sizeof(rtree_coord_t);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  CU_ASSERT_EQUAL(state_rect_size(state), 4 * float_size);
  state_destroy(state);
}

void test_state_unit_sphere_volume(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  rtree_coord_t volume = state_unit_sphere_volume(state);
  state_destroy(state);

  rtree_coord_t eps;

  switch (sizeof(rtree_coord_t))
    {
    case 8: eps = DBL_EPSILON; break;
    case 4: eps = FLT_EPSILON; break;
    default:
      CU_FAIL("strange rtree_coord_t size");
    }

  CU_ASSERT_DOUBLE_EQUAL(volume, M_PI, 2 * eps * M_PI);
}

void test_state_split(void)
{
  state_t *state;

  state = state_new(2, RTREE_SPLIT_LINEAR);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  CU_ASSERT_EQUAL(state_split(state), RTREE_SPLIT_LINEAR);
  state_destroy(state);

  state = state_new(2, RTREE_SPLIT_QUADRATIC);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  CU_ASSERT_EQUAL(state_split(state), RTREE_SPLIT_QUADRATIC);
  state_destroy(state);

  state = state_new(2, RTREE_SPLIT_GREENE);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  CU_ASSERT_EQUAL(state_split(state), RTREE_SPLIT_GREENE);
  state_destroy(state);
}

void test_state_node_page(void)
{
  for (size_t i = 0 ; i < 16 ; i++)
    {
      state_flags_t flags = RTREE_NODE_PAGE(i) | RTREE_SPLIT_LINEAR;
      state_t *state = state_new(2, flags);

      CU_ASSERT_PTR_NOT_NULL_FATAL(state);
      CU_ASSERT_EQUAL(state_node_page(state), i);
      state_destroy(state);
    }

  for (size_t i = 16 ; i < 256 ; i++)
    {
      errno = 0;

      state_flags_t flags = RTREE_NODE_PAGE(i) | RTREE_SPLIT_LINEAR;
      state_t *state = state_new(2, flags);

      if (state != NULL)
        {
          CU_ASSERT_EQUAL(state_node_page(state), i);
          state_destroy(state);
        }
      else
        CU_ASSERT_EQUAL(errno, EINVAL);
    }
}

void test_state_identical_null(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  CU_ASSERT_TRUE(state_identical(NULL, NULL));
  CU_ASSERT_FALSE(state_identical(state, NULL));
  CU_ASSERT_FALSE(state_identical(NULL, state));

  state_destroy(state);
}

void test_state_identical_dims_equal(void)
{
  for (size_t dims = 1 ; dims < 10 ; dims++)
    {
      state_t
        *a = state_new(dims, 0),
        *b = state_new(dims, 0);
      CU_ASSERT_PTR_NOT_NULL_FATAL(a);
      CU_ASSERT_PTR_NOT_NULL_FATAL(b);

      CU_ASSERT_TRUE(state_identical(a, b));

      state_destroy(a);
      state_destroy(b);
    }
}

void test_state_identical_dims_unequal(void)
{
  state_t
    *a = state_new(2, 0),
    *b = state_new(3, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(a);
  CU_ASSERT_PTR_NOT_NULL_FATAL(b);

  CU_ASSERT_FALSE(state_identical(a, b));

  state_destroy(a);
  state_destroy(b);
}

void test_state_bytes_null(void)
{
  CU_ASSERT_EQUAL(state_bytes(NULL), 0);
}

void test_state_bytes_non_null(void)
{
  state_t *state = state_new(2, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(state);
  CU_ASSERT_NOT_EQUAL(state_bytes(state), 0);

  state_destroy(state);
}
