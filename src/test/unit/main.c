#include <stdio.h>
#include <stdlib.h>

#include <CUnit/Basic.h>
#include <CUnit/Automated.h>

#include "tests.h"

// #define NO_STDERR

#ifdef NO_STDERR
#include <fcntl.h>
#include <unistd.h>
#endif

int main(void)
{
  CU_BasicRunMode mode = CU_BRM_VERBOSE;
  CU_ErrorAction error_action = CUEA_IGNORE;
  setvbuf(stdout, NULL, _IONBF, 0);

  if (CU_initialize_registry())
    {
      fprintf(stderr,"failed to initialise registry\n");
      return EXIT_FAILURE;
    }

  tests_load();
  CU_basic_set_mode(mode);
  CU_set_error_action(error_action);

#ifdef NO_STDERR

  int devnull = open("/dev/null", O_RDWR|O_CREAT|O_APPEND, 0600);
  int save_err = dup(fileno(stderr));

  if (dup2(devnull, fileno(stderr)) == -1)
    {
      printf("failed to redirect stderr\n");
      return EXIT_FAILURE;
    }

#endif

  int status = CU_basic_run_tests();

  CU_set_output_filename("tmp/cunit");
  CU_list_tests_to_file();
  CU_automated_run_tests();

#ifdef NO_STDERR

  fflush(stderr);
  close(devnull);

  dup2(save_err, fileno(stderr));
  close(save_err);

#endif

  int nfail = CU_get_number_of_failures();

  printf("\nSuite %s: %d failed\n",
         (status == 0 ? "OK" : "errored"),
         nfail);

  CU_cleanup_registry();

  return (nfail > 0 ? EXIT_FAILURE : EXIT_SUCCESS);
}
