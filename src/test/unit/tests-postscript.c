#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-postscript.h"
#include "assert-ps-valid.h"
#include "assert-ps-bbox.h"

#include <private/node.h>
#include <private/state.h>
#include <private/rtree.h>
#include <private/postscript.h>
#include <rtree/error.h>

#include <unistd.h>

#ifdef WITH_JSON

CU_TestInfo tests_postscript[] = {
  {"NULL arguments", test_postscript_null},
  {"invalid dimension", test_postscript_bad_dim},
  {"empty", test_postscript_empty},
  {"not-empty", test_postscript_nonempty},
  {"split", test_postscript_split},
  {"under-styled", test_postscript_under_styled},
  {"style, valid", test_postscript_style_valid},
  {"style, absent fill", test_postscript_style_absent_fill},
  {"style, absent stroke", test_postscript_style_absent_stroke},
  {"style, invalid key", test_postscript_style_invalid_key},
  {"style, invalid grey", test_postscript_style_invalid_grey},
  {"bbox of shifted tree", test_postscript_bbox_shift},
  {"bbox for width specified", test_postscript_bbox_width},
  {"bbox for height specified", test_postscript_bbox_height},
  CU_TEST_INFO_NULL
};

#include <private/json.h>

#include "fixture.h"
#include "fixture-style.h"
#include "fixture-json.h"

void test_postscript_null(void)
{
  state_t *state = state_new(2, RTREE_DEFAULT);
  CU_ASSERT_PTR_NOT_NULL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL(node);

  postscript_style_t *style = fixture_style("valid.style");
  CU_ASSERT_PTR_NOT_NULL_FATAL(style);

  rtree_postscript_t opt = {
    .style = style,
    .axis = axis_width,
    .extent = 200
  };

  int err;

  err = postscript_write(NULL, node, &opt, NULL);
  CU_ASSERT_EQUAL(err, RTREE_ERR_INVAL);

  err = postscript_write(state, NULL, &opt, NULL);
  CU_ASSERT_EQUAL(err, RTREE_ERR_INVAL);

  err = postscript_write(state, node, NULL, NULL);
  CU_ASSERT_EQUAL(err, RTREE_ERR_INVAL);

  postscript_style_destroy(style);
  node_destroy(state, node);
  state_destroy(state);
}

void test_postscript_bad_dim(void)
{
  state_t *state = state_new(3, RTREE_DEFAULT);
  CU_ASSERT_PTR_NOT_NULL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL(node);

  postscript_style_t *style = fixture_style("valid.style");
  CU_ASSERT_PTR_NOT_NULL_FATAL(style);

  rtree_postscript_t opt = {
    .style = style,
    .axis = axis_width,
    .extent = 200
  };

  int err = postscript_write(state, node, &opt, NULL);

  CU_ASSERT_EQUAL(err, RTREE_ERR_DIMS);

  postscript_style_destroy(style);
  node_destroy(state, node);
  state_destroy(state);
}

void test_postscript_empty(void)
{
  state_t *state = state_new(2, RTREE_DEFAULT);
  CU_ASSERT_PTR_NOT_NULL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL(node);

  postscript_style_t *style = fixture_style("valid.style");
  CU_ASSERT_PTR_NOT_NULL_FATAL(style);

  rtree_postscript_t opt = {
    .style = style,
    .axis = axis_width,
    .extent = 200
  };

  int err = postscript_write(state, node, &opt, NULL);

  CU_ASSERT_EQUAL(err, RTREE_ERR_EMPTY);

  postscript_style_destroy(style);
  node_destroy(state, node);
  state_destroy(state);
}

void test_postscript_nonempty(void)
{
  state_t *state = state_new(2, RTREE_DEFAULT);
  CU_ASSERT_PTR_NOT_NULL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL(root);

  rtree_coord_t rect[] = {0, 0, 200, 100};
  node_t *node = node_add_rect(state, 1, rect, root, 0);
  CU_ASSERT_PTR_EQUAL_FATAL(root, node);

  char path[] = "tmp/test-postscript-nonempty-XXXXXX";
  int fd = mkstemp(path);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  FILE *stream = fdopen(fd, "w");
  CU_ASSERT_PTR_NOT_NULL(stream);

  postscript_style_t *style = fixture_style("valid.style");
  CU_ASSERT_PTR_NOT_NULL_FATAL(style);

  rtree_postscript_t opt = {
    .style = style,
    .axis = axis_width,
    .extent = 200
  };

  int err = postscript_write(state, root, &opt, stream);
  fclose(stream);

  CU_ASSERT_EQUAL(err, RTREE_OK);
  CU_ASSERT_PS_VALID(path);

  unlink(path);

  postscript_style_destroy(style);
  node_destroy(state, node);
  state_destroy(state);
}

void test_postscript_split(void)
{
  FILE *stream;

  stream = fixture_json_stream("split-decimated.json");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = json_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  fclose(stream);

  postscript_style_t *style = fixture_style("valid.style");
  CU_ASSERT_PTR_NOT_NULL_FATAL(style);

  rtree_postscript_t opt = {
    .style = style,
    .axis = axis_width,
    .extent = 200
  };

  char path[] = "tmp/test-postscript-split-XXXXXX";
  int fd = mkstemp(path);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  stream = fdopen(fd, "w");
  CU_ASSERT_PTR_NOT_NULL(stream);

  int err = postscript_write(rtree->state, rtree->root, &opt, stream);

  fclose(stream);

  CU_ASSERT_EQUAL(err, RTREE_OK);
  CU_ASSERT_PS_VALID(path);

  unlink(path);

  postscript_style_destroy(style);
  rtree_destroy(rtree);
}

/* a height 2 tree with a height 1 style should not error */

void test_postscript_under_styled(void)
{
  FILE *stream;

  stream = fixture_json_stream("split-decimated.json");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = json_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  fclose(stream);

  postscript_style_t *style = fixture_style("one-level.style");
  CU_ASSERT_PTR_NOT_NULL_FATAL(style);

  rtree_postscript_t opt = {
    .style = style,
    .axis = axis_width,
    .extent = 200
  };

  char path[] = "tmp/test-postscript-under-styled-XXXXXX";
  int fd = mkstemp(path);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  stream = fdopen(fd, "w");
  CU_ASSERT_PTR_NOT_NULL(stream);

  int err = postscript_write(rtree->state, rtree->root, &opt, stream);

  fclose(stream);

  CU_ASSERT_EQUAL(err, RTREE_OK);
  CU_ASSERT_PS_VALID(path);

  unlink(path);

  postscript_style_destroy(style);
  rtree_destroy(rtree);
}


void test_postscript_style_valid(void)
{
  postscript_style_t *style = fixture_style("valid.style");
  CU_ASSERT_PTR_NOT_NULL_FATAL(style);
  CU_ASSERT_EQUAL(style->n, 2);
  CU_ASSERT_PTR_NOT_NULL_FATAL(style->array);
  CU_ASSERT_EQUAL(style->array[0].fill.colour.model, model_grey);
  CU_ASSERT_EQUAL(style->array[0].fill.colour.grey[0], 1.0);
  CU_ASSERT_EQUAL(style->array[0].stroke.colour.model, model_grey);
  CU_ASSERT_EQUAL(style->array[0].stroke.colour.grey[0], 0.5);
  CU_ASSERT_EQUAL(style->array[0].stroke.width, 1.0);
  CU_ASSERT_EQUAL(style->array[1].fill.colour.model, model_rgb);
  CU_ASSERT_EQUAL(style->array[1].fill.colour.rgb[0], 0.0);
  CU_ASSERT_EQUAL(style->array[1].fill.colour.rgb[1], 0.5);
  CU_ASSERT_EQUAL(style->array[1].fill.colour.rgb[2], 1.0);
  CU_ASSERT_EQUAL(style->array[1].stroke.colour.model, model_cmyk);
  CU_ASSERT_EQUAL(style->array[1].stroke.colour.cmyk[0], 1.0);
  CU_ASSERT_EQUAL(style->array[1].stroke.colour.cmyk[1], 0.5);
  CU_ASSERT_EQUAL(style->array[1].stroke.colour.cmyk[2], 0.0);
  CU_ASSERT_EQUAL(style->array[1].stroke.colour.cmyk[3], 0.5);
  CU_ASSERT_EQUAL(style->array[1].stroke.width, 0.25);
  postscript_style_destroy(style);
}

void test_postscript_style_absent_fill(void)
{
  postscript_style_t *style = fixture_style("absent-fill.style");
  CU_ASSERT_PTR_NOT_NULL_FATAL(style);
  postscript_style_destroy(style);
}

void test_postscript_style_absent_stroke(void)
{
  postscript_style_t *style = fixture_style("absent-stroke.style");
  CU_ASSERT_PTR_NOT_NULL_FATAL(style);
  postscript_style_destroy(style);
}

void test_postscript_style_invalid_key(void)
{
  postscript_style_t *style = fixture_style("invalid-key.style");
  CU_ASSERT_PTR_NULL(style);
}

void test_postscript_style_invalid_grey(void)
{
  postscript_style_t *style = fixture_style("invalid-grey.style");
  CU_ASSERT_PTR_NULL(style);
}

void case_postscript_bbox_shift(const char *fixture)
{
  FILE *stream;
  double bbox_expected[] = {0, 0, 100, 100};

  stream = fixture_json_stream(fixture);
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = json_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  fclose(stream);

  char path[] = "tmp/test-postscript-bbox-shfted-XXXXXX";

  int fd = mkstemp(path);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  stream = fdopen(fd, "w");
  CU_ASSERT_PTR_NOT_NULL(stream);

  postscript_style_t *style = fixture_style("valid.style");
  CU_ASSERT_PTR_NOT_NULL(style);

  rtree_postscript_t opt = {
     .style = style,
     .axis = axis_width,
     .extent = 100
  };
  int err = postscript_write(rtree->state, rtree->root, &opt, stream);

  postscript_style_destroy(style);
  fclose(stream);

  CU_ASSERT_EQUAL(err, RTREE_OK);
  CU_ASSERT_PS_BBOX(path, bbox_expected, 1e-4);

  unlink(path);
  rtree_destroy(rtree);
}

void test_postscript_bbox_shift(void)
{
  case_postscript_bbox_shift("shift00.json");
  case_postscript_bbox_shift("shift10.json");
  case_postscript_bbox_shift("shift01.json");
  case_postscript_bbox_shift("shift11.json");
}

/*
  for a simple 2:1 R-tree, plot with the extent of axis being 200pt; the
  resulting plot should have bounding box passed in the second argument.
*/

static void case_postscript_bbox_extent(extent_axis_t axis, double *bbox)
{
  state_t *state = state_new(2, RTREE_DEFAULT);
  CU_ASSERT_PTR_NOT_NULL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL(root);

  rtree_coord_t rect[] = {1, 1, 3, 2};
  node_t *node = node_add_rect(state, 1, rect, root, 0);
  CU_ASSERT_PTR_EQUAL_FATAL(root, node);

  char path[] = "tmp/test-postscript-bbox-extent-XXXXXX";
  int fd = mkstemp(path);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  FILE *stream = fdopen(fd, "w");
  CU_ASSERT_PTR_NOT_NULL(stream);

  postscript_style_t *style = fixture_style("valid.style");
  CU_ASSERT_PTR_NOT_NULL_FATAL(style);

  rtree_postscript_t opt = {
    .style = style,
    .axis = axis,
    .extent = 200
  };

  int err = postscript_write(state, root, &opt, stream);
  fclose(stream);

  CU_ASSERT_EQUAL(err, RTREE_OK);
  CU_ASSERT_PS_VALID(path);
  CU_ASSERT_PS_BBOX(path, bbox, 1e-4);

  unlink(path);

  postscript_style_destroy(style);
  node_destroy(state, node);
  state_destroy(state);
}

void test_postscript_bbox_width(void)
{
  double bbox[4] = {0, 0, 200, 100};
  case_postscript_bbox_extent(axis_width, bbox);
}

void test_postscript_bbox_height(void)
{
  double bbox[4] = {0, 0, 400, 200};
  case_postscript_bbox_extent(axis_height, bbox);
}

#else

CU_TestInfo tests_postscript[] = { CU_TEST_INFO_NULL };

#endif
