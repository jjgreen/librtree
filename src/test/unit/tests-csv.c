#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-csv.h"

#include "fixture.h"
#include "fixture-csv.h"

#include <private/csv.h>

CU_TestInfo tests_csv[] = {
  {"read, NULL stream", test_csv_read_null_stream},
  {"read, short line", test_csv_read_short_line},
  {"read, simple", test_csv_read_simple},
  {"read, line-endings", test_csv_read_line_endings},
  {"write, NULL rtree", test_csv_write_null_rtree},
  {"write, NULL stream", test_csv_write_null_stream},
  {"write, empty", test_csv_write_empty},
  {"write, non-empty", test_csv_write_nonempty},
  CU_TEST_INFO_NULL
};

void test_csv_read_null_stream(void)
{
  errno = 0;
  rtree_t *rtree = rtree_csv_read(NULL, 2, 0);
  CU_ASSERT_PTR_NULL(rtree);
  CU_ASSERT_EQUAL(errno, EINVAL);
}

void test_csv_read_short_line(void)
{
  int m, n = 1024;
  char path[n];

  m = fixture(path, n, "csv", "short-line.csv");
  CU_ASSERT_TRUE_FATAL(m < n);

  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = rtree_csv_read(stream, 2, 0);
  CU_ASSERT_PTR_NULL(rtree);

  fclose(stream);
}

void test_csv_read_simple(void)
{
  int m, n = 1024;
  char path[n];

  m = fixture(path, n, "csv", "split-vertical.csv");
  CU_ASSERT_TRUE_FATAL(m < n);

  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = rtree_csv_read(stream, 2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  fclose(stream);
  rtree_destroy(rtree);
}

void test_csv_read_line_endings(void)
{
  int m, n = 1024;
  char path[n];
  FILE *stream;

  m = fixture(path, n, "csv", "simple-lf.csv");
  CU_ASSERT_TRUE_FATAL(m < n);
  stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  rtree_t *rtree_lf = rtree_csv_read(stream, 2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree_lf);
  fclose(stream);

  m = fixture(path, n, "csv", "simple-crlf.csv");
  CU_ASSERT_TRUE_FATAL(m < n);
  stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  rtree_t *rtree_crlf = rtree_csv_read(stream, 2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree_crlf);
  fclose(stream);

  CU_ASSERT_EQUAL(rtree_identical(rtree_lf, rtree_crlf), true);

  rtree_destroy(rtree_lf);
  rtree_destroy(rtree_crlf);
}

void test_csv_write_null_rtree(void)
{
  FILE *stream = tmpfile();
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  CU_ASSERT_NOT_EQUAL(rtree_csv_write(NULL, stream), 0);
  fclose(stream);
}

void test_csv_write_null_stream(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_NOT_EQUAL(rtree_csv_write(rtree, NULL), 0);
  rtree_destroy(rtree);
}

void test_csv_write_empty(void)
{
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  FILE *stream = tmpfile();
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  CU_ASSERT_EQUAL(rtree_csv_write(rtree, stream), 0);
  CU_ASSERT_EQUAL(ftell(stream), 0);
  fclose(stream);
  rtree_destroy(rtree);
}

void test_csv_write_nonempty(void)
{
  FILE *stream = tmpfile();
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  rtree_t *rtree = fixture_csv_rtree(2, 0, "split-vertical.csv");
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(rtree_csv_write(rtree, stream), 0);
  CU_ASSERT_TRUE(ftell(stream) > 0);
  fclose(stream);
  rtree_destroy(rtree);
}
