#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-bindex.h"

#include <private/bindex.h>
#include <rtree/error.h>

CU_TestInfo tests_bindex[] = {
  {"new", test_bindex_new},
  {"set, domain error", test_bindex_set_domain},
  {"set, range error", test_bindex_set_range},
  {"set, valid", test_bindex_set_valid},
  {"get, domain error", test_bindex_get_domain},
  {"get, unset", test_bindex_get_unset},
  {"get, set", test_bindex_get_set},
  {"first unset, all", test_bindex_first_unset_all},
  {"first unset, word 1", test_bindex_first_unset_word1},
  {"first unset, word 2", test_bindex_first_unset_word2},
  {"first unset, none", test_bindex_first_unset_none},
  {"next unset, all", test_bindex_next_unset_all},
  {"next unset, word 1", test_bindex_next_unset_word1},
  {"next unset, word 2", test_bindex_next_unset_word2},
  {"next unset, none", test_bindex_next_unset_none},
  CU_TEST_INFO_NULL
};

void test_bindex_new(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);
  bindex_destroy(bindex);
}

void test_bindex_set_domain(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);
  CU_ASSERT_EQUAL(bindex_set(bindex, 75, 0), RTREE_ERR_DOM);
  bindex_destroy(bindex);
}

void test_bindex_set_range(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);
  CU_ASSERT_EQUAL(bindex_set(bindex, 16, 2), RTREE_ERR_INVAL);
  bindex_destroy(bindex);
}

void test_bindex_set_valid(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);
  CU_ASSERT_EQUAL(bindex_set(bindex, 74, 1), RTREE_OK);
  bindex_destroy(bindex);
}

void test_bindex_get_domain(void)
{
  errno = 0;
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_EQUAL(bindex_get(bindex, 76), 0);
  CU_ASSERT_EQUAL(errno, EDOM);
}

void test_bindex_get_unset(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);
  for (size_t i = 0 ; i < 75 ; i++)
    CU_ASSERT_EQUAL(bindex_get(bindex, i), 0);
  bindex_destroy(bindex);
}

void test_bindex_get_set(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);
  for (size_t i = 0 ; i < 75 ; i++)
    {
      CU_ASSERT_EQUAL(bindex_get(bindex, i), 0);
      CU_ASSERT_EQUAL(bindex_set(bindex, i, 1), 0);
      CU_ASSERT_EQUAL(bindex_get(bindex, i), 1);
      CU_ASSERT_EQUAL(bindex_set(bindex, i, 0), 0);
      CU_ASSERT_EQUAL(bindex_get(bindex, i), 0);
    }
  bindex_destroy(bindex);
}

/* bindex_first_unset */

void test_bindex_first_unset_all(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);
  for (size_t i = 0 ; i < 75 ; i++) bindex_set(bindex, i, 1);
  CU_ASSERT_EQUAL(bindex_first_unset(bindex), 75);
  bindex_destroy(bindex);
}

void test_bindex_first_unset_word1(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);
  for (size_t i = 0 ; i < 50 ; i++) bindex_set(bindex, i, 1);
  for (size_t i = 51 ; i < 75 ; i++) bindex_set(bindex, i, 1);
  CU_ASSERT_EQUAL(bindex_first_unset(bindex), 50);
  bindex_destroy(bindex);
}

void test_bindex_first_unset_word2(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);
  for (size_t i = 0 ; i < 70 ; i++) bindex_set(bindex, i, 1);
  for (size_t i = 71 ; i < 75 ; i++) bindex_set(bindex, i, 1);
  CU_ASSERT_EQUAL(bindex_first_unset(bindex), 70);
  bindex_destroy(bindex);
}

void test_bindex_first_unset_none(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);
  CU_ASSERT_EQUAL(bindex_first_unset(bindex), 0);
  bindex_destroy(bindex);
}

/* bindex_next_unset */

void test_bindex_next_unset_all(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);

  for (size_t i = 0 ; i < 75 ; i++)
    bindex_set(bindex, i, 1);

  for (size_t i = 0 ; i < 75 ; i++)
    CU_ASSERT_EQUAL(bindex_next_unset(bindex, i), 75);

  bindex_destroy(bindex);
}

void test_bindex_next_unset_word1(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);

  for (size_t i = 0 ; i < 50 ; i++)
    bindex_set(bindex, i, 1);
  for (size_t i = 51 ; i < 75 ; i++)
    bindex_set(bindex, i, 1);

  for (size_t i = 0 ; i <= 50 ; i++)
    CU_ASSERT_EQUAL(bindex_next_unset(bindex, i), 50);
  for (size_t i = 51 ; i < 75 ; i++)
    CU_ASSERT_EQUAL(bindex_next_unset(bindex, i), 75);

  bindex_destroy(bindex);
}

void test_bindex_next_unset_word2(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);

  for (size_t i = 0 ; i < 70 ; i++)
    bindex_set(bindex, i, 1);
  for (size_t i = 71 ; i < 75 ; i++)
    bindex_set(bindex, i, 1);

  for (size_t i = 0 ; i <= 70 ; i++)
    CU_ASSERT_EQUAL(bindex_next_unset(bindex, i), 70);
  for (size_t i = 71 ; i < 75 ; i++)
    CU_ASSERT_EQUAL(bindex_next_unset(bindex, i), 75);

  bindex_destroy(bindex);
}

void test_bindex_next_unset_none(void)
{
  bindex_t *bindex = bindex_new(75);
  CU_ASSERT_PTR_NOT_NULL_FATAL(bindex);

  for (size_t i = 0 ; i < 75 ; i++)
    CU_ASSERT_EQUAL(bindex_next_unset(bindex, i), i);

  bindex_destroy(bindex);
}
