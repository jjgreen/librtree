/*
  rtree-query.h
  Copyright (c) J.J. Green 2020
*/

#ifndef RTREE_QUERY_H
#define RTREE_QUERY_H

#include <stdbool.h>
#include <stdlib.h>

typedef struct
{
  bool verbose;
  struct {
    const char *query, *output, *tree;
  } path;
} rtree_query_t;

int rtree_query(rtree_query_t*);

#endif
