#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "rtree-query.h"

#include <rtree.h>

#include <stdio.h>
#include <errno.h>
#include <string.h>

typedef struct
{
  rtree_t *rtree;
  struct {
    FILE *query, *output;
  } stream;
  rtree_query_t *opt;
} workspace_t;

static void fprintf_errno(const char *message)
{
  if (errno != 0)
    fprintf(stderr, "%s (%s)\n", message, strerror(errno));
  else
    fprintf(stderr, "%s\n", message);
}

static rtree_t* rtree_read_stream(FILE *stream)
{
  long start;

  if ((start = ftell(stream)) == -1)
    {
      fprintf_errno("failed to get stream position");
      return NULL;
    }

  char magic[4];

  if (fread(magic, 1, 4, stream) != 4)
    {
      fprintf(stderr, "failed read header\n");
      return NULL;
    }

  rtree_t* (*reader)(FILE*) = NULL;

  if (memcmp(magic, "BSRt", 4) == 0)
    reader = rtree_bsrt_read;
  else
    reader = rtree_json_read;

  if (fseek(stream, start, SEEK_SET) != 0)
    {
      fprintf_errno("failed seek of rtree stream");
      return NULL;
    }

  rtree_t *rtree;

  if ((rtree = reader(stream)) == NULL)
    {
      fprintf_errno("failed read of rtree");
      return NULL;
    }

  return rtree;
}

static rtree_t* rtree_read_path(const char *path)
{
  FILE *stream;
  rtree_t *rtree = NULL;

  if ((stream = fopen(path, "r")) != NULL)
    {
      rtree = rtree_read_stream(stream);
      fclose(stream);
    }

  return rtree;
}

typedef struct
{
  FILE *stream;
  size_t id;
} write_data_t;

static int writer(rtree_id_t id, void *arg)
{
  write_data_t *data = arg;
  fprintf(data->stream, "%zi,%zi\r\n", data->id, id);
  return 0;
}

#define LINE_LEN 0x1000

static int rtree_query4(workspace_t *work)
{
  char line[LINE_LEN];
  rtree_t *rtree = work->rtree;
  size_t dims = rtree_dims(rtree);
  rtree_coord_t rect[2 * dims];
  write_data_t write_data = { .stream = work->stream.output };

  while (fgets(line, LINE_LEN, work->stream.query) != NULL)
    {
      char *tok;

      if ((tok = strtok(line, ",")) == NULL)
        continue;

      write_data.id = strtoul(tok, NULL, 0);

      for (size_t i = 0 ; i < 2 * dims - 1 ; i++)
        {
          if ((tok = strtok(NULL, ",")) == NULL)
            return RTREE_ERR_CSVPARSE;
          rect[i] = strtod(tok, NULL);
        }

      if ((tok = strtok(NULL, ",\n\r")) == NULL)
        return RTREE_ERR_CSVPARSE;
      rect[2 * dims - 1] = strtod(tok, NULL);

      if (rtree_search(rtree, rect, writer, &write_data) != 0)
        return RTREE_ERR_CSVPARSE;
    }

  return 0;
}

static int rtree_query3(workspace_t *work)
{
  if (work->opt->path.output)
    {
      FILE *stream;

      if ((stream = fopen(work->opt->path.output, "w")) != NULL)
        {
          work->stream.output = stream;
          int err = rtree_query4(work);
          fclose(stream);
          return err;
        }
      else
        {
          fprintf_errno("failed to open output");
          return 1;
        }
    }
  else
    {
      work->stream.output = stdout;
      return rtree_query4(work);
    }
}

static int rtree_query2(workspace_t *work)
{
  if (work->opt->path.query)
    {
      FILE *stream;

      if ((stream = fopen(work->opt->path.query, "r")) != NULL)
        {
          work->stream.query = stream;
          int err = rtree_query3(work);
          fclose(stream);
          return err;
        }
      else
        {
          fprintf_errno("failed read of input");
          return 1;
        }
    }
  else
    {
      work->stream.query = stdin;
      return rtree_query3(work);
    }
}

int rtree_query(rtree_query_t *opt)
{
  rtree_t *rtree;

  if ((rtree = rtree_read_path(opt->path.tree)) != NULL)
    {
      workspace_t work = { .rtree = rtree, .opt = opt };
      int err = rtree_query2(&work);
      rtree_destroy(rtree);
      return err;
    }

  return 1;
}
