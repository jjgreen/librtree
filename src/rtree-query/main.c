#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include "options.h"
#include "rtree-query.h"

int wrap(struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  rtree_query_t opt;

  opt.verbose = info->verbose_given;
  opt.path.output = (info->output_given ? info->output_arg : NULL);

  if (!opt.path.output && opt.verbose)
    {
      fprintf(stderr, "verbosity suppressed (<stdout> is used for output)\n");
      opt.verbose = false;
    }

  if (! info->tree_given)
    {
      fprintf(stderr, "R-tree to search required (--tree)\n");
      return EXIT_FAILURE;
    }

  opt.path.tree = info->tree_arg;

  switch (info->inputs_num)
    {
    case 0:
      opt.path.query = NULL;
      break;

    case 1:
      opt.path.query = info->inputs[0];
      break;

    default:
      fprintf(stderr, "sorry, only one file at a time\n");
      return EXIT_FAILURE;
    }

  if (opt.verbose)
    printf("This is rtree-query (version %s)\n", VERSION);

  int err = rtree_query(&opt);

  if (err)
    fprintf(stderr, "failed\n");
  else if (opt.verbose)
    {
      printf("matches written to %s\n",
             (opt.path.output ? opt.path.output : "<stdout>"));
      printf("done.\n");
    }

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;

  if (options(argc, argv, &info) != 0)
    {
      fprintf(stderr, "failed to parse command line\n");
      return EXIT_FAILURE;
    }

  int err = wrap(&info);

  options_free(&info);

  return err;
}
