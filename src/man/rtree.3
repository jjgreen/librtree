'\" t
.\"     Title: RTREE
.\"    Author: [see the "AUTHOR" section]
.\" Generator: DocBook XSL Stylesheets v1.79.2 <http://docbook.sf.net/>
.\"      Date:  7 April 2024
.\"    Manual: Library Functions Manual
.\"    Source: librtree 1.3.2
.\"  Language: English
.\"
.TH "RTREE" "3" "7 April 2024" "librtree 1\&.3\&.2" "Library Functions Manual"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
rtree \- build and query R\-tree spatial indices
.SH "SYNOPSIS"
.sp
.ft B
.nf
#include <rtree\&.h>
    
.fi
.ft
.HP \w'rtree_t*\ rtree_new('u
.BI "rtree_t* rtree_new(size_t\ " "dim" ", unsigned\ " "flags" ");"
.HP \w'void\ rtree_destroy('u
.BI "void rtree_destroy(rtree_t\ *" "rtree" ");"
.HP \w'rtree_t*\ rtree_clone('u
.BI "rtree_t* rtree_clone(const\ rtree_t\ *" "rtree" ");"
.HP \w'bool\ rtree_empty('u
.BI "bool rtree_empty(const\ rtree_t\ *" "rtree" ");"
.HP \w'rtree_height_t\ rtree_height('u
.BI "rtree_height_t rtree_height(const\ rtree_t\ *" "rtree" ");"
.HP \w'int\ rtree_envelope('u
.BI "int rtree_envelope(const\ rtree_t\ *" "rtree" ", rtree_coord_t\ *" "rect" ");"
.HP \w'int\ rtree_add_rect('u
.BI "int rtree_add_rect(rtree_t\ *" "rtree" ", rtree_id_t\ " "id" ", const\ rtree_coord_t\ *" "rect" ");"
.HP \w'int\ rtree_update('u
.BI "int rtree_update(rtree_t\ *" "rtree" ", rtree_update_t\ *" "f" ", void\ *" "context" ");"
.HP \w'int\ rtree_search('u
.BI "int rtree_search(const\ rtree_t\ *" "rtree" ", const\ rtree_coord_t\ *" "rect" ", rtree_search_t\ *" "f" ", void\ *" "context" ");"
.HP \w'int\ rtree_csv_write('u
.BI "int rtree_csv_write(const\ rtree_t\ *" "rtree" ", FILE\ *" "stream" ");"
.HP \w'rtree_t*\ rtree_csv_read('u
.BI "rtree_t* rtree_csv_read(FILE\ *" "stream" ", size_t\ " "dim" ", unsigned\ " "flags" ");"
.HP \w'int\ rtree_json_write('u
.BI "int rtree_json_write(const\ rtree_t\ *" "rtree" ", FILE\ *" "stream" ");"
.HP \w'rtree_t*\ rtree_json_read('u
.BI "rtree_t* rtree_json_read(FILE\ *" "stream" ");"
.HP \w'int\ rtree_bsrt_write('u
.BI "int rtree_bsrt_write(const\ rtree_t\ *" "rtree" ", FILE\ *" "stream" ");"
.HP \w'rtree_t*\ rtree_bsrt_read('u
.BI "rtree_t* rtree_bsrt_read(FILE\ *" "stream" ");"
.HP \w'int\ rtree_postscript('u
.BI "int rtree_postscript(const\ rtree_t\ *" "rtree" ", const\ rtree_postscript_t\ *" "options" ", FILE\ *" "stream" ");"
.HP \w'bool\ rtree_identical('u
.BI "bool rtree_identical(const\ rtree_t\ *" "rtree0" ", const\ rtree_t\ *" "rtree1" ");"
.HP \w'size_t\ rtree_bytes('u
.BI "size_t rtree_bytes(const\ rtree_t\ *" "rtree" ");"
.HP \w'size_t\ rtree_dims('u
.BI "size_t rtree_dims(const\ rtree_t\ *" "rtree" ");"
.HP \w'size_t\ rtree_page_size('u
.BI "size_t rtree_page_size(const\ rtree_t\ *" "rtree" ");"
.HP \w'size_t\ rtree_node_size('u
.BI "size_t rtree_node_size(const\ rtree_t\ *" "rtree" ");"
.HP \w'size_t\ rtree_rect_size('u
.BI "size_t rtree_rect_size(const\ rtree_t\ *" "rtree" ");"
.HP \w'size_t\ rtree_branch_size('u
.BI "size_t rtree_branch_size(const\ rtree_t\ *" "rtree" ");"
.HP \w'size_t\ rtree_branching_factor('u
.BI "size_t rtree_branching_factor(const\ rtree_t\ *" "rtree" ");"
.HP \w'double\ rtree_unit_sphere_volume('u
.BI "double rtree_unit_sphere_volume(const\ rtree_t\ *" "rtree" ");"
.HP \w'const\ char*\ rtree_strerror('u
.BI "const char* rtree_strerror(int\ " "err" ");"
.SH "DESCRIPTION"
.PP
On including the header
rtree\&.h
the following functions become available:
.SS "rtree_new"
.PP
The
\fBsize_t\fR
argument specifies the dimension of the (empty) R\-tree returned; it should be a positive integer\&. The
\fIflags\fR
is the bitwise\-or of one or more of
.sp
.RS 4
.ie n \{\
\h'-04' 1.\h'+01'\c
.\}
.el \{\
.sp -1
.IP "  1." 4.2
.\}
One of
\fBRTREE_SPLIT_LINEAR\fR,
\fBRTREE_SPLIT_QUADRATIC\fR
or
\fBRTREE_SPLIT_GREENE\fR; these determine the splitting strategy, the linear strategy is faster to build, the quadratic produces a better\-quality R\-tree which is faster to query\&. The Greene variant is a quadratic\-time alternative to the Guttman original\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 2.\h'+01'\c
.\}
.el \{\
.sp -1
.IP "  2." 4.2
.\}
The macro
\fBRTREE_NODE_PAGE(n)\fR
which sets the nodes\-per\-page value
\fBn\fR\&. This value can affect performance quite dramatically, particularly build time\&. A value which is too large would result in an infeasible branching factor for the R\-tree and will case the function to error with
\fIerrno\fR
set to
\fBEINVAL\fR\&.
.sp
A value of
\fBn\fR
of zero is permitted and the default; in this case the function will choose a good value based on heuristics\&. You may get better performance for your use\-case by manual experimentation, but zero is a good place to start\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 3.\h'+01'\c
.\}
.el \{\
.sp -1
.IP "  3." 4.2
.\}
The
\fBRTREE_DEFAULT\fR
value which is (and will remain) zero\&. When used alone it will select the default splitting strategy and the heuristic nodes\-per\-page\&.
.RE
.PP
On error, returns
\fBNULL\fR, and may set the
\fIerrno\fR
variable to indicate the cause\&.
.SS "rtree_destroy"
.PP
Releases the memory used by the
\fBrtree_t*\fR
which is its argument, as returned by
\fBrtree_new\fR
or
\fBrtree_clone\fR\&. A
\fBNULL\fR
argument is acceptable here\&.
.SS "rtree_clone"
.PP
Creates and returns a deep\-copy of the
\fBrtree_t*\fR
which is its argument\&. On failure
\fBNULL\fR
is returned\&. The copy shares no data with the original and should be destroyed separately\&.
.PP
A typical use case for this function is a sequence of R\-trees which share a common part\&. Once can create the latter, then clone and add the specific part for each instance\&.
.PP
On error, returns
\fBNULL\fR, and may set the
\fIerrno\fR
variable to indicate the cause\&.
.SS "rtree_empty"
.PP
Returns
\fBtrue\fR
if the tree has no leaf nodes\&.
.PP
Equivalent to a test that the
\fBrtree_height\fR
is zero, but does not need to iterate over the whole tree\&.
.SS "rtree_height"
.PP
Returns the
\fIheight\fR
of the R\-tree in the usual mathematical sense\&. A newly created
\fBrtree_t*\fR
has height zero (it has a root\-node with no descendants)\&.
.PP
The return values is of type
\fBrtree_height_t\fR, an unsigned integer type\&.
.SS "rtree_envelope"
.PP
For a non\-\fBNULL\fR
and non\-empty
\fBrtree_t*\fR, writes the minimal rectangle which contains all of the rectangles so\-far inserted: the bounding box\&.
.PP
If the first argument is
\fBNULL\fR
or empty, then the function returns non\-zero and does not modify the passed
\fIrect\fR
argument\&.
.SS "rtree_add_rect"
.PP
Adds a rectangle (or higher\-dimensional cuboid) to the R\-tree structure specified by the first
\fBrtree_t*\fR
argument\&.
.PP
The second
\fIid\fR
argument is an integer used to identify the rectangle\&. The type
\fBrtree_id_t\fR
of this integer is determined at compile\-time\&. By default it is the largest unsigned integer type which is no larger than a pointer, so a
\fBuint32_t\fR
on x86 and
\fBuint64_t\fR
on the AMD64 architecture\&. One can, however, override the default at compilation by defining the
\fBRTREE_ID_TYPE\fR
constant\&.
.PP
It is anticipated that the
\fIid\fR
will be used as an index for application specific data to which this rectangle relates, but this is entirely at the discretion of the caller, the library makes no use of the value, treating it as payload\&. In particular, the value may be non\-unique and may be zero\&.
.PP
The final argument is (a pointer to) an array of
\fBrtree_coord_t\fR, twice as many as the dimension of the R\-tree\&. The floating point values are the lower extents of each dimension, followed by the upper extents of each dimension; these values are copied into the tree\&. By default the
\fBrtree_coord_t\fR
type is
\fBfloat\fR, but this can be overridden at compilation by defining the
\fBRTREE_COORD_TYPE\fR
constant to the desired floating\-point type\&.
.SS "rtree_update"
.PP
Modifies the
\fIrtree\fR
which is the first argument according to the function
\fIf\fR
which is the second\&. This function, which has prototype
.HP \w'int\ f('u
.BI "int f(rtree_id_t\ " "id" ", rtree_coord_t\ *" "rect" ", void\ *" "context" ");"
.PP
is applied to all of the rectangles in the tree, it is passed the
\fIid\fR
as used in the constructor, and may modify the
\fIrect\fR
which is its second argument\&. If the return value is non\-zero, then the iteration will abort and return
\fBRTREE_ERR_USER\fR\&. Otherwise, once all rectangles have been updated, the spatial index will be updated and queries against the tree should be correct\&.
.sp
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNotes\fR
.RS 4
.sp
.RS 4
.ie n \{\
\h'-04' 1.\h'+01'\c
.\}
.el \{\
.sp -1
.IP "  1." 4.2
.\}
The update of the spatial index does not modify the
\fIstructure\fR
of the tree, it just recalculates the bounding rectangles for the parent branches down to the root (which is much faster than rebuilding the tree)\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 2.\h'+01'\c
.\}
.el \{\
.sp -1
.IP "  2." 4.2
.\}
If the rectangles are only slightly modified, then the resulting tree should be nearly as efficient as the original in spatial searching, but if the rectangles are substantially changed then one could find a collapse in search speed, one should instead rebuild the tree from scratch\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 3.\h'+01'\c
.\}
.el \{\
.sp -1
.IP "  3." 4.2
.\}
If the function does not return
\fBRTREE_OK\fR
(in particular, if it returns
\fBRTREE_ERR_USER\fR) then subsequent spatial queries against the tree may not be correct\&.
.RE
.RE
.SS "rtree_search"
.PP
This function searches the
\fBrtree_t*\fR
which is its first argument for rectangles which intersect the the second
\fIrect\fR
argument\&. For each found, the callback function
\fIf\fR, whose prototype is
.HP \w'int\ f('u
.BI "int f(rtree_id_t\ " "id" ", void\ *" "context" ");"
.PP
is called\&. The
\fIid\fR
is the rectangle identifier as given in
\fBrtree_add_rectangle\fR, and the
\fIcontext\fR
is the
\fBvoid*\fR
passed as the final argument to
\fBrtree_search\fR\&.
.PP
The function
\fIf\fR
should return zero to continue the search, non\-zero to terminate it\&. In the latter case it is this value which
\fBrtree_search\fR
returns\&.
.SS "rtree_csv_write"
.PP
Writes the ids and rectangles from the
\fIrtree_t*\fR
in the format described by
\m[blue]\fB\fBrtree-csv\fR(5)\fR\m[]
to the
\fIstream\fR\&. One could then rebuild an equivalent
\fIrtree_t*\fR
with the
\fBrtree_csv_read\fR
function (equivalent in that the rebuilt
\fIrtree_t*\fR
will give the same results on search, but may fail on call to
\fBrtree_identical\fR
since the rectangle may not have been inserted in the same order)\&.
.SS "rtree_csv_read"
.PP
Reads rectangles from the
\fIstream\fR
in the format described by
\m[blue]\fB\fBrtree-csv\fR(5)\fR\m[]
writing them to the
\fIrtree_t*\fR
returned\&. One also passes the dimension of the tree as
\fBsize_t\fR; not guessing this value from the CSV file allows the function to handle files with extra columns\&. The final
\fIflags\fR
arguments is as described in
\fBrtree_new\fR, above\&.
.PP
On error, returns
\fBNULL\fR, and may set the
\fIerrno\fR
variable to indicate the cause\&.
.SS "rtree_json_write"
.PP
Writes the R\-tree in the first argument to the
\fIstream\fR
in the second in the JSON format as described in
\m[blue]\fB\fBrtree-json\fR(5)\fR\m[]\&.
.SS "rtree_json_read"
.PP
Reads JSON in the format as described in
\m[blue]\fB\fBrtree-json\fR(5)\fR\m[]
from
\fIstream\fR
and returns the R\-tree that it describes\&.
.PP
Note that the read will fail if the JSON input was created on a system with a different page\-size, or where the library was compiled with a different float size\&. In this case one would need to build the R\-tree afresh\&.
.PP
On error, returns
\fBNULL\fR, and may set the
\fIerrno\fR
variable to indicate the cause\&. In particular, the value
\fBENOSYS\fR
indicates that the library was compiled without JSON support, while
\fBEINVAL\fR
that the input file is incompatible with the library (a later version, of differing page\-size and so on)\&.
.SS "rtree_bsrt_write"
.PP
Writes the R\-tree in the first argument to the
\fIstream\fR
in the second in the BSRT (binary serialised R\-tree) format as described in
\m[blue]\fB\fBrtree-bsrt\fR(5)\fR\m[]\&.
.SS "rtree_bsrt_read"
.PP
Reads BSRT in the format as described in
\m[blue]\fB\fBrtree-bsrt\fR(5)\fR\m[]
from
\fIstream\fR
and returns the R\-tree that it describes\&.
.PP
Note that the read will fail if the BSRT input was created on a system with a different page\-size, or where the library was compiled with a different float size\&. In this case one would need to build the R\-tree afresh\&.
.PP
On error, returns
\fBNULL\fR, and may set the
\fIerrno\fR
variable to indicate the cause\&. In particular, the value
\fBENOSYS\fR
indicates that the library was compiled without BSRT support, while
\fBEINVAL\fR
that the input file is incompatible with the library (a later version, of differing page\-size and so on)\&.
.SS "rtree_postscript"
.PP
Plots the
\fIrtree\fR
argument as PostScript to the specified
\fIstream\fR\&. The appearance of the plot is controlled by the
\fIoptions\fR
argument\&.
.SS "rtree_identical"
.PP
Two R\-trees are identical if they have the same nodes, branches and leaves
\fIin the same order\fR\&. This function returns a Boolean indicating whether its arguments are identical or not\&. It is mainly intended for tests (of
\fBrtree_clone\fR
and the serialisation functions for example)\&.
.SS "rtree_bytes"
.PP
The memory allocated by the R\-tree, or zero if the argument is
\fBNULL\fR\&.
.SS "rtree_dims"
.PP
The dimension of the R\-tree, or zero if the argument is
\fBNULL\fR\&.
.SS "rtree_page_size"
.PP
The hardware page\-size used by R\-tree, typically 1024 on 32\-bit hardware, 4096 on 64\-bit, or zero if the argument is
\fBNULL\fR\&.
.SS "rtree_node_size"
.PP
The size in bytes of an R\-tree node, or zero if the argument is
\fBNULL\fR\&.
.SS "rtree_rect_size"
.PP
The size in bytes of an R\-tree rectangle, or zero if the argument is
\fBNULL\fR\&.
.SS "rtree_branch_size"
.PP
The size in bytes of an R\-tree branch, or zero if the argument is
\fBNULL\fR\&.
.SS "rtree_branching_factor"
.PP
The number of child\-nodes for each parent\-node in the R\-tree (see the notes in
\fBrtree_new\fR), or zero if the argument is
\fBNULL\fR\&.
.SS "rtree_unit_sphere_volume"
.PP
The volume of the unit sphere in the dimension of the R\-tree, or zero if the argument is
\fBNULL\fR\&.
.SS "rtree_strerror"
.PP
For library functions which return an integer, zero represents success, non\-zero an error\&. A readable description of the error can be obtained by passing the value to this function\&. The returned string is a constant: it should not be freed\&.
.PP
The one exception to this is
\fBrtree_search\fR, since that returns a user\-defined value\&.
.SH "AUTHOR"
.PP
J\&.J\&. Green,
<j\&.j\&.green@gmx\&.co\&.uk>\&.
.SH "SEE ALSO"
.PP
\m[blue]\fB\fBrtree-csv\fR(5)\fR\m[],
\m[blue]\fB\fBrtree-json\fR(5)\fR\m[],
\m[blue]\fB\fBrtree-bsrt\fR(5)\fR\m[],
\m[blue]\fB\fBerrno\fR(3)\fR\m[]\&.
