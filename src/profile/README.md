Profiling
=========

Some scripts for profiling with [gprof][1].  We profile the build
via the `rtree-build` script applied to a datafile in `csv`, this
is a large file (100,000 lines, 4.5 MB) so stored using the git
LFS mechanism.  The results are written to per-version files in
the `prof` directory.

The `bench` directory contains per-version timings for the same build
and query, run several times _without profiling_ (that makes the code
much slower), these numbers are averaged to give the timings in `build.csv`
and `query.csv`.

The data here is only of interest to developers, so the directory
is excluded from the library distribution archives.

Note (2020-06-29, v0.7.0): there seems to be something untoward with
the numbers here. Inspecting the numbers for `node_add_rect` and
`node_add_rect2`, it seems that prior to v0.7.0, the number of calls
is the wrong way around, `node_add_rect` is only called by the
`rtree_add_rect` function and that is called once per line of the CVS,
so there should be 100001 calls.  But the profile lists `node_add_rect2`
as being called 100001 times, `node_add_rect` as being 46759628 times.
In v0.7.0, both have 100001 calls and identical time-per-call.
Possibly this is because they rather similar names.

Nodes per page
--------------

For version 0.9 we introduce the idea of nodes-per-page (where it is
understood that a node includes all of its branches).  Previous to
this, one node was used per page of memory; the reason being that
although this facility was present in the original Guttman-Green code,
via a `#define` at least, the code itself had this value hard-coded as
one.  I had assumed that this was a vestige of an experiment which had
proved unsuccessful, a bad assumption to make since a value of eight
more than doubles build speed, almost doubles query speed.  The table
following is for the benchmarks for dimension two.

| nodes-per-page | build time | query time |
|----------------|------------|------------|
| 1              | 0.883125   | 1.01250    |
| 2              | 0.578125   | 0.70375    |
| 4              | 0.429375   | 0.61875    |
| 8              | 0.345625   | 0.61250    |
| 16             | 0.310000   | 0.60125    |

[1]: http://sourceware.org/binutils/docs/gprof/
