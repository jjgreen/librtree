/*
  rtree-eps.h
  Copyright (c) J.J. Green 2020
*/

#ifndef RTREE_EPS_H
#define RTREE_EPS_H

#include <stdbool.h>
#include <rtree/types.h>
#include <rtree/extent.h>

typedef enum { format_auto, format_json, format_bsrt } format_t;

typedef struct
{
  struct {
    const char *tree, *eps;
  } path;
  const char *style;
  rtree_coord_t extent, margin;
  extent_axis_t axis;
  format_t format;
  bool verbose;
} rtree_eps_t;

int rtree_eps(rtree_eps_t*);
int rtree_list_styles(bool);

#endif
