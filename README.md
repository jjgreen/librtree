librtree
--------

Implementation of the [R-tree][0] algorithm by A. Guttman, updated by
Melinda Green. Retrieved from ([1]), July 2019.  This rather old code
(Green reports updating it to ANSI C) has been rewritten in C11 with an
extensive test-suite to make a small, reasonably portable self-contained
library.  For use in smaller projects.

For documentation and stable releases, see the [project homepage][7].


### Features

The same underlying algorithms are implemented, and we have some features
which seem desirable for a modern library:

- Re-entrant, no global variables at all
- Dimension set at run-time rather than compile-time
- Extensive test-suite (with CUnit), used for CI ([4])
- Serialisation to/from custom binary format or JSON
- Output to PostScript in the dimension-two case
- Builds a 100,000 rectangle tree in around a third of a second


### Building

Quite standard:

    ./configure
    make
    sudo make install

If you do not have the [Jansson library][5] (or do not want JSON
support) add the `--disable-json` option to the `configure`.

To run the test-suite (which requires the CUnit library)

    ./configure --enable-unit-tests
    make
    make test

See `./configure --help` for additional features.


### OS-specific dependencies

Debian and derivatives (Ubuntu, Mint, ...)

- build: `libjansson-dev`
- test: `libcunit1-dev`, `valgrind`, `ghostscript`, `shellcheck`
- develop: `clang-tools`, `cppcheck`, `gengetopt`, `xsltproc`,
  `jq`, `cloc`, `gcovr`

RedHat and derivatives (RHEL, CentOS, Fedora, ...)

- build: `jansson-devel`
- test: `CUnit`, `valgrind`, `ghostscript`, `ShellCheck`


### Licence

The licence of the original files, according to ([1]), is that

> You're completely free to use them for any purpose whatsoever.
> All I ask is that if you find one to be particularly valuable,
> then consider sending feedback.

Given this, I have released the rewrite under the MIT licence (see
the file LICENCE).


[0]: https://en.wikipedia.org/wiki/R-tree
[1]: http://superliminal.com/sources/sources.htm
[4]: https://gitlab.com/jjg/librtree/pipelines
[5]: http://www.digip.org/jansson/
[7]: https://jjg.gitlab.io/en/code/librtree/
[8]: https://trac.osgeo.org/geos
