RUBBISH = *~
CONFIG = config.cache config.log config.status libtool

all:
	$(MAKE) -C src all

install:
	$(MAKE) -C src install

uninstall:
	$(MAKE) -C src uninstall

test:
	$(MAKE) -C src test

clean:
	$(MAKE) -C src clean
	$(RM) $(RUBBISH)

spotless:
	$(MAKE) -C src spotless
	$(RM) $(CONFIG) $(RUBBISH)
	$(RM) autom4te.cache/*
	$(RM) -d autom4te.cache

.PHONY: all install uninstall test clean spotless

# maintainer targets

# this needs to be done once on first cloning the repo, only if you
# plan to develop the library: it symlinks files in .hooks (which
# is version controlled) to .git/hooks (which is not).

git-setup-hooks:
	bin/git-setup-hooks

.PHONY: git-setup-hooks

# note that ./configure creates the version file VERSION, the variable
# $(VERSION) reads that file, so one should not use that variable
# until ./configure has been invoked, it will contain the old version.

VERSION = $(shell cat VERSION)

# coverage

coverage-prepare:
	./configure \
		--enable-coverage \
		--enable-unit-tests \
		--enable-acceptance-tests \
		--enable-json \
		--enable-bsrt \
		--enable-simd \
		--enable-ctzl
	$(MAKE) all test

coverage-run:
	$(MAKE) -C src coverage

coverage-show:
	$(MAKE) -C src coverage-show

coverage-clean: spotless

.PHONY: coverage-prepare coverage-run coverage-show coverage-clean

update-coverage: coverage-prepare coverage-run coverage-clean

.PHONY: update-coverage

# coverity

update-coverity:
	bin/coverity-build
	bin/coverity-upload
	bin/coverity-clean

.PHONY: update-coverity

# git targets used by release

git-release-commit:
	git add -u
	git commit -m $(VERSION)
	git push origin master

git-create-tag:
	bin/git-release-tag $(VERSION)

git-push-tag:
	git push origin v$(VERSION)

.PHONY: git-release-commit git-create-tag git-push-tag

# release targets proper

update-autoconf:
	aclocal --output config/aclocal.m4
	autoconf
	autoheader

update-assets:
	./configure
	bin/version-assets
	$(MAKE)
	$(MAKE) spotless

update-depends:
	./configure
	$(MAKE) -C src/librtree update-depend
	$(MAKE) spotless

update: update-autoconf update-assets

release: update git-release-commit git-create-tag git-push-tag

.PHONY: update-autoconf update-assets update-depends update release

# the tgz distribution

DIST = librtree-$(VERSION)

dist:
	tar -C .. \
	  --transform s/^librtree/$(DIST)/ \
	  --exclude-from .distignore \
	  -zpcvf ../$(DIST).tar.gz librtree

.PHONY: dist
