# generated automatically by aclocal 1.16.5 -*- Autoconf -*-

# Copyright (C) 1996-2021 Free Software Foundation, Inc.

# This file is free software; the Free Software Foundation
# gives unlimited permission to copy and/or distribute it,
# with or without modifications, as long as this notice is preserved.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.

m4_ifndef([AC_CONFIG_MACRO_DIRS], [m4_defun([_AM_CONFIG_MACRO_DIRS], [])m4_defun([AC_CONFIG_MACRO_DIRS], [_AM_CONFIG_MACRO_DIRS($@)])])
m4_include([config/m4/ax_c_float_words_bigendian.m4])
m4_include([config/m4/ax_check_compile_flag.m4])
m4_include([config/m4/ax_check_docbook_xslt.m4])
m4_include([config/m4/ax_ext.m4])
m4_include([config/m4/ax_gcc_builtin.m4])
m4_include([config/m4/ax_gcc_x86_avx_xgetbv.m4])
m4_include([config/m4/ax_gcc_x86_cpuid.m4])
m4_include([config/m4/ax_prog_xsltproc.m4])
m4_include([config/m4/ax_python_module.m4])
m4_include([config/m4/libtool.m4])
m4_include([config/m4/ltoptions.m4])
m4_include([config/m4/ltsugar.m4])
m4_include([config/m4/ltversion.m4])
m4_include([config/m4/lt~obsolete.m4])
