Changelog
---------

### 1.3.2, 07-04-2024

- New function `rtree_envelope`, gives the bounding rectangle for
  the entire tree.
- New function `rtree_csv_write`, writes the ids and bounding
  rectangles for the leaf-nodes of the tree in CSV format.

### 1.3.1, 19-10-2023

- The library exports are now minimal in that only the features
  used in the official API are exported, no change in the latter.

### 1.3.0, 16-10-2023

- The bulk of the "unofficial" API (functions outside `rtree.h`) is
  now private; some of that functionality (in particular, information
  about the `state_t`) is still accessible _via_  wrapper scripts in
  the official API, see `rtree_dims` and so-on documented at the end
  of the manual **rtree**(3).

### 1.2.0, 08-04-2023

- Renamed `colour_t` and `style_t` from `postscript.h` to have a
  `postscript` prefix (since these are exported, and were too
  generic, inviting name collisions).  Minor version bump since
  this changes the API (even if slightly).

### 1.1.4, 16-03-2023

- Added `rtree_empty`, faster than checking the height is zero
- The file `package.c` is generated at configure, but depends only
  on per-version values so is only updated on release), `postscript.c`
  now depends on the values in the former.
- Rename internal header `limits.h` to `bounds.h` (to avoid collision
  with `<limits.h>`)
- Implement the `uninstall` target

### 1.1.3, 23-02-2023

- Replace MIN/MAX macros by C99 type-generic `fmin`/`fmax`
- Coverage support
- Update BATS to 1.9.0
- Remove the `librtree/private` directory, all private headers
  are now in the `librtree` directory
- Added **librtree-update** for embedding the library in other
  projects

### 1.1.2, 17-07-2022

- Refactor of the `branches.c` API, modest speed bump
- Further fuzz-testing support and fixes
- Fix error-case memory leaks found by Coverity, to who, many thanks
- Removed several unused public declarations from `rectf.h`

### 1.1.1, 05-06-2022

- Update BATS to 1.7.0
- Scripts for fuzz-testing
- Fix memory errors on read of hostile BSRT

### 1.1.0, 25-05-2022

- Update BATS to 1.6.0
- Use `__builtin_ctzl` (if available) for faster bit-index operations

### 1.0.9, 06-02-2022

- Replace several calls to `malloc` by allocation on the stack.
- Rework the memory testing resulting in more coverage.

### 1.0.8, 06-01-2022

- Remove switch on dimension in heavily-used rectangle functions
  in the new `rectf.c` module; this speeds-up tree-build by several
  percent.
- Fix overuse of memory in `bindex.c`

### 1.0.7, 28-11-2021

- Minimise header includes with **include-what-you-use**(1)
- The repository's shell-scripts now pass **shellcheck**(1)
- BATS (acceptance testing framework) updated to 1.5.0
- At configure, we now check for Ruby (used for some development
  targets, not required for build or install).

### 1.0.6, 17-06-2021

- Added "the Greene split", an alternative splitting strategy as
  devised by D. Greene in the 1989 paper "An Implementation and
  Performance Analysis of Spatial Data Access Methods".
- The `rtree_build` function now accepts `RTREE_SPLIT_GREENE`
  as an option.
- The **rtree-build**(1) program now has a switch **--split** with
  argument **linear**, **quadratic** or **greene**, this replacing
  the **--linear** and **--quadratic** flags.
- The `rect_merge` and `rect_combine` functions of `rtree/rect.h`
  now return _void_ rather than _int_.
- Acceptance tests (with the excellent BATS)

### 1.0.5, 12-06-2021

- Removed dependency on **libcsv**, replacing the functionality with
  a hand-written CSV parser.  This simplifies the code and speeds up
  reading from CSV a little.

### 1.0.4, 29-05-2021

- The library is now built with **libtool**, which should improve
  the portability of the shared-object version.  Reports of success
  or failure installing on non-Linux systems would be most welcome.

### 1.0.3, 24-05-2021

- Add the `rtree_bytes` function.

### 1.0.2, 23-05-2021

- Add package metadata, declared in `rtree/package.h`
- Add `rtree_alloc` and `rtree_init` functions, and re-implement
  `rtree_new` with those, mainly for the benefit of the Ruby
  extension.

### 1.0.1, 12-05-2021

- Fix floating-point types in **rtree-eps**(1).
- Added a **--enable-shared** option to **configure** which creates a
  shared library.

### 1.0.0, 03-05-2021

- Fixes for several defects found by Coverity (to who, many thanks).

### 0.9.5, 18-04-2021

- Autoconf checks for Python and the GMPy2 module, if found enables
  the `spvol.c` build-target (only needed for development).
- All public headers are `extern "C"` for the benefit of C++.

### 0.9.4, 08-02-2021

- The **rtree-eps**(1) style listing is now sorted alphabetically.
- Rework of maintenance targets.
- Fix installation of header files.

### 0.9.3, 29-12-2020

- Previews for style files.
- Several new Brewer styles.
- Added the **--list-styles** option to **rtree-eps**(1).

### 0.9.2, 24-12-2020

- Tests for bounding-boxes of output of `rtree_postscript`.
- The **rtree-eps**(1) program accepts common units in the **--margin**
  and **--width** options.
- Several new **rtree-style**(5) files based on the colour schemes
  by Cynthia Brewer.
- The **rtree-eps**(1) program now has a **--height** option allowing
  the output plot-height to be specified (instead of the width).

### 0.9.1, 14-12-2020

- Fix for incorrect bounding-box in **rtree-eps**(1).

### 0.9.0, 08-12-2020

- Introduces the control of "number of nodes (and their branches)
  per page of memory", or "node-page"; choosing the value appropriately
  gives a major performance boost in R-tree build and search times.
- In the C API, pass the value to the state constructor via the macro
  `RTREE_NODE_PAGE(n)`, bitwise-or with existing values.  The node-page
  value `n` (at most 255) is ideally a small power of two: 2, 4, 8, …,
  but non-dyadic arguments are accepted; a value too large will lead to
  an R-tree branching factor less than two, which is infeasible, so
  errors with an `EINVAL`.
- The **rtree-build**(1) program accesses this facility via the new
  **--nodes-per-branch** option.
- The JSON output now includes the node-size, and expects it to be
  present in the input.  JSON files generated with an earlier version
  of the library will need to be regenerated.
- The BSRT output is incremented to version 2 which includes the node-size;
  the library now writes only version 2, but reads both versions 1 and 2.

### 0.8.7, 23-11-2020

- Added the new program **rtree-query**(1), bulk query tool.

### 0.8.6, 19-11-2020

- Replaced `ucarray.c`, an array of unsigned char, by `bindex.c`,
  a bit-array.

### 0.8.5, 11-10-2020

- Added a JSON schema for style-files and a development target to
  check all distributed style-files against this schema.
- Extend SIMD to the dimension 3, 4 float and dimension 2 double
  cases (using the 256-bit AVX registers).
- Rework the PostScript "style" structure to more-naturally represent
  the stroke and fill.

### 0.8.4, 20-09-2020

- Support for the CMYK colour-space in EPS plots of trees.
- Reworking of the `rtree_height` function which is now robust against
  branch deletion.

### 0.8.3, 31-08-2020

- Added `rtree_identical`, mainly for use in tests.
- Replace switch on `sizeof` by conditional compilation.
- The binary serialisation code should now compile on OSX, Windows
  and most BSD variants (see `endianness.h`).
- Several header files only used internally are now not installed.

### 0.8.2, 24-08-2020

- Unit tests now pass with `RTREE_COORD_TYPE` double.
- Serialisation to/from the binary `bsrt` format: see **rtree-bsrt**(5)
  for details on the format.

### 0.8.1, 06-07-2020

- Implemented SIMD calculation of `rect_combine` in the dimension-two
  case.  Configure with the new **--enable-simd** option to use this (of
  course, you need to have a CPU which supports these instructions).

### 0.8.0, 02-07-2020

- Implemented fast reindexing (fixing issue #21) in the `rtree_update`
  function.  Useful for updating the index when the rectangle-set has
  changed only slightly, as in multi-body simulations.

### 0.7.0, 07-06-2020

- Implemented contiguous branches (fixing issue #22), replacing linear
  searches through a page of memory with a constant-time calculation.
- Consequently, branch-sizes are reduced, the tree is more-compact.

### 0.6.3, 07-06-2020

- More inlining of accessors and micro-optimisations, I think that's
  most of the low-hanging fruit picked.

### 0.6.2, 05-06-2020

- Added profiling (included only in the git repository).
- The `state_t` structure is now public with accessors reimplemented
  as inline functions, improves build times by around 15%.
- Optimisation of spherical volume calculation.

### 0.6.1, 27-05-2020

- The `level` and `count` attributes of `node_t` are now `uint16_t`,
  still much larger that any conceivable value.  The `rtree_height`
  now returns a value of this type.  They are aliased to the types
  `rtree_height_t` and `rtree_level_t`.
- The `node_new()` function returns a (pointer to) `node_t` with the
  level initialised to zero, rather than -1, this simplifies several
  typical use-case.
- Tidying of `node.c`
- The file `array.c` (implementing a managed array of unsigned char)
  is renamed to `ucarray.c`.
- A new style file, `jjg2.style`, for the plot on the package homepage.


### 0.6.0, 21-05-2020

- Added PostScript plotting to the library (see `postscript.h`),
  includes a "style file" format (describing fill and stroke
  colours), a couple of example style-files included.
- New program **rtree-eps**(1) creates EPS plots from R-tree JSON.
- Testing now requires **ghostscript**, we use it to test that the
  plotting produces valid PostScript.
- Fix for the linear splitting strategy.
- The quadratic splitting strategy is now the default in the
  **rtree-build**(1) program.

### 0.5.2, 10-05-2020

- Added `rtree_strerror` for description of errors

### 0.5.1, 10-05-2020

- Several errors now set `errno`

### 0.5.0, 09-05-2020

- Added the quadratic splitting strategy
- The functions `rtree_new` and `rtree_csv_read` gain an extra
  argument, "flags", which selects linear or quadratic splitting.
  To migrate existing code and maintain existing behaviour, add
  `RTREE_DEFAULT` (which is zero) as the extra argument.
- The **rtree-build**(1) program gains options **--split-linear**
  and **--split-quadratic** which select the strategy.

### 0.4.2, 04-05-2020

- Fix and regression test for bad count in `rtree_json_read`

### 0.4.1, 03-05-2020

- Documentation fixes

### 0.4.0, 03-05-2020

- Added `rtree_json_read`, for deserialisation (fixing issue #6)
- Modified the signature of `rtree_csv_read`, for consistency
- Modified the format of the JSON serialisation, so that it more
  closely models the internal structure of the tree (this makes
  reading and writing of JSON simpler)
- Rationalised the test-setup (fixing issue #16)

### 0.3.0, 29-04-2020

- Added **rtree**(3) (fixing issue #13)
- Several changes to the API, hence the minor version bump
- Types for tree id and coordinate are now configurable
- Added "extern C" for the benefit of C++

### 0.2.4, 27-04-2020

- Added `rtree_clone`, a deep copy of an R-tree (fixing issue #15)

### 0.2.3, 26-04-2020

- Remove `branch_new`, allocating on the stack instead of the heap
  (fixing issue #4)

### 0.2.2, 26-04-2020

- A **valgrind** test for `rtree_json_write`
- Merge branches into node (fixing issue #3)
